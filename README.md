# Caelum Labs Lorena Playground

## Getting started

Connects to local substrate via websocket.

### Local

To use the camera with an insecure site (such as http://localhost:3001) you'll need to enable the following in Chrome:

* [chrome://flags/#unsafely-treat-insecure-origin-as-secure](chrome://flags/#unsafely-treat-insecure-origin-as-secure)

```shell script
git clone git@gitlab.com:caelum-tech/lorena/lorena-playground.git
cd lorena-playground
yarn install
./node_modules/@caelum-tech/zenroom-lib/bin/zenroom_modules.sh
yarn start
```

### Docker

#### Docker-compose

```dockerfile
version: "3.7"
services:
    ...
    client:
        build:
            context: ../lorena-playground/
            dockerfile: Dockerfile
        volumes:
            - ./client:/app
            # One-way volume to use node_modules from inside image - /app/node_modules
```

Run:

```shell script
sudo docker-compose up --build client
```

## Zenroom

In order to use zenroom, please follow the following steps. From tutorial [Make ❤️ with Zenroom and Javascript (part 3)](https://www.dyne.org/using-zenroom-with-javascript-react-part3/)

```bash
yarn add zenroom
cd public
ln -s ../node_modules/zenroom/dist/lib/zenroom.wasm .
```

After we will need to change `node_modules/zenroom/dist/lib/zenroom.js` (`src/zenroomApi/zenroom_modules.sh` does this for you) file from this:

```javascript
var wasmBinaryFile = 'zenroom.wasm';
if (!isDataURI(wasmBinaryFile)) {
  wasmBinaryFile = locateFile(wasmBinaryFile);
}
```

to

```javascript
var wasmBinaryFile = '/zenroom.wasm';
if (!isDataURI(wasmBinaryFile)) {
  // wasmBinaryFile = locateFile(wasmBinaryFile);
}
```

>NOTE: Only two changes
>
>1. Add `'/'` to `'zenroom.wasm'` -> `'/zenroom.wasm'`.
>2. Comment line in `if` statement.

Finally run your app.

```bash
yarn start
```
