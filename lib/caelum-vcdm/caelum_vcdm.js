import * as wasm from './caelum_vcdm_bg.wasm';

let WASM_VECTOR_LEN = 0;

let cachedTextEncoder = new TextEncoder('utf-8');

const encodeString = (typeof cachedTextEncoder.encodeInto === 'function'
    ? function (arg, view) {
    return cachedTextEncoder.encodeInto(arg, view);
}
    : function (arg, view) {
    const buf = cachedTextEncoder.encode(arg);
    view.set(buf);
    return {
        read: arg.length,
        written: buf.length
    };
});

let cachegetUint8Memory = null;
function getUint8Memory() {
    if (cachegetUint8Memory === null || cachegetUint8Memory.buffer !== wasm.memory.buffer) {
        cachegetUint8Memory = new Uint8Array(wasm.memory.buffer);
    }
    return cachegetUint8Memory;
}

function passStringToWasm(arg) {

    let len = arg.length;
    let ptr = wasm.__wbindgen_malloc(len);

    const mem = getUint8Memory();

    let offset = 0;

    for (; offset < len; offset++) {
        const code = arg.charCodeAt(offset);
        if (code > 0x7F) break;
        mem[ptr + offset] = code;
    }

    if (offset !== len) {
        if (offset !== 0) {
            arg = arg.slice(offset);
        }
        ptr = wasm.__wbindgen_realloc(ptr, len, len = offset + arg.length * 3);
        const view = getUint8Memory().subarray(ptr + offset, ptr + len);
        const ret = encodeString(arg, view);

        offset += ret.written;
    }

    WASM_VECTOR_LEN = offset;
    return ptr;
}

let cachegetInt32Memory = null;
function getInt32Memory() {
    if (cachegetInt32Memory === null || cachegetInt32Memory.buffer !== wasm.memory.buffer) {
        cachegetInt32Memory = new Int32Array(wasm.memory.buffer);
    }
    return cachegetInt32Memory;
}

let cachedTextDecoder = new TextDecoder('utf-8', { ignoreBOM: true, fatal: true });

function getStringFromWasm(ptr, len) {
    return cachedTextDecoder.decode(getUint8Memory().subarray(ptr, ptr + len));
}
/**
*
* # Utils interface
* @param {string} s
* @returns {string}
*/
export function create_hash(s) {
    const retptr = 8;
    const ret = wasm.create_hash(retptr, passStringToWasm(s), WASM_VECTOR_LEN);
    const memi32 = getInt32Memory();
    const v0 = getStringFromWasm(memi32[retptr / 4 + 0], memi32[retptr / 4 + 1]).slice();
    wasm.__wbindgen_free(memi32[retptr / 4 + 0], memi32[retptr / 4 + 1] * 1);
    return v0;
}

function getArrayU8FromWasm(ptr, len) {
    return getUint8Memory().subarray(ptr / 1, ptr / 1 + len);
}
/**
* @param {string} s
* @returns {Uint8Array}
*/
export function create_u8a_hash(s) {
    const retptr = 8;
    const ret = wasm.create_u8a_hash(retptr, passStringToWasm(s), WASM_VECTOR_LEN);
    const memi32 = getInt32Memory();
    const v0 = getArrayU8FromWasm(memi32[retptr / 4 + 0], memi32[retptr / 4 + 1]).slice();
    wasm.__wbindgen_free(memi32[retptr / 4 + 0], memi32[retptr / 4 + 1] * 1);
    return v0;
}

/**
* @param {string} hexa
* @returns {Uint8Array}
*/
export function hex_to_u8a(hexa) {
    const retptr = 8;
    const ret = wasm.hex_to_u8a(retptr, passStringToWasm(hexa), WASM_VECTOR_LEN);
    const memi32 = getInt32Memory();
    const v0 = getArrayU8FromWasm(memi32[retptr / 4 + 0], memi32[retptr / 4 + 1]).slice();
    wasm.__wbindgen_free(memi32[retptr / 4 + 0], memi32[retptr / 4 + 1] * 1);
    return v0;
}

function passArray8ToWasm(arg) {
    const ptr = wasm.__wbindgen_malloc(arg.length * 1);
    getUint8Memory().set(arg, ptr / 1);
    WASM_VECTOR_LEN = arg.length;
    return ptr;
}
/**
* @param {Uint8Array} vec
* @returns {string}
*/
export function u8a_to_hex(vec) {
    const retptr = 8;
    const ret = wasm.u8a_to_hex(retptr, passArray8ToWasm(vec), WASM_VECTOR_LEN);
    const memi32 = getInt32Memory();
    const v0 = getStringFromWasm(memi32[retptr / 4 + 0], memi32[retptr / 4 + 1]).slice();
    wasm.__wbindgen_free(memi32[retptr / 4 + 0], memi32[retptr / 4 + 1] * 1);
    return v0;
}

const heap = new Array(32);

heap.fill(undefined);

heap.push(undefined, null, true, false);

let heap_next = heap.length;

function addHeapObject(obj) {
    if (heap_next === heap.length) heap.push(heap.length + 1);
    const idx = heap_next;
    heap_next = heap[idx];

    heap[idx] = obj;
    return idx;
}

function getObject(idx) { return heap[idx]; }

function dropObject(idx) {
    if (idx < 36) return;
    heap[idx] = heap_next;
    heap_next = idx;
}

function takeObject(idx) {
    const ret = getObject(idx);
    dropObject(idx);
    return ret;
}
/**
* @param {any} claim
* @param {any} proof
* @returns {boolean}
*/
export function verify_claim(claim, proof) {
    const ret = wasm.verify_claim(addHeapObject(claim), addHeapObject(proof));
    return ret !== 0;
}

/**
*
* # WebAssembly gateway for Claims
*/
export class VClaim {

    static __wrap(ptr) {
        const obj = Object.create(VClaim.prototype);
        obj.ptr = ptr;

        return obj;
    }

    free() {
        const ptr = this.ptr;
        this.ptr = 0;

        wasm.__wbg_vclaim_free(ptr);
    }
    /**
    * @returns {VClaim}
    */
    constructor() {
        const ret = wasm.vclaim_new();
        return VClaim.__wrap(ret);
    }
    /**
    * @param {any} json
    * @returns {VClaim}
    */
    static fromJSON(json) {
        const ret = wasm.vclaim_fromJSON(addHeapObject(json));
        return VClaim.__wrap(ret);
    }
    /**
    * @returns {any}
    */
    toJSON() {
        const ret = wasm.vclaim_toJSON(this.ptr);
        return takeObject(ret);
    }
    /**
    * @param {Uint8Array} keys
    * @returns {string}
    */
    sign(keys) {
        const retptr = 8;
        const ret = wasm.vclaim_sign(retptr, this.ptr, passArray8ToWasm(keys), WASM_VECTOR_LEN);
        const memi32 = getInt32Memory();
        const v0 = getStringFromWasm(memi32[retptr / 4 + 0], memi32[retptr / 4 + 1]).slice();
        wasm.__wbindgen_free(memi32[retptr / 4 + 0], memi32[retptr / 4 + 1] * 1);
        return v0;
    }
    /**
    * @param {any} proof
    * @returns {boolean}
    */
    verify(proof) {
        const ret = wasm.vclaim_verify(this.ptr, addHeapObject(proof));
        return ret !== 0;
    }
    /**
    * @returns {any}
    */
    getCredentialStatus() {
        const ret = wasm.vclaim_getCredentialStatus(this.ptr);
        return takeObject(ret);
    }
    /**
    * @param {any} cs
    * @returns {VClaim}
    */
    setCredentialStatus(cs) {
        const ptr = this.ptr;
        this.ptr = 0;
        const ret = wasm.vclaim_setCredentialStatus(ptr, addHeapObject(cs));
        return VClaim.__wrap(ret);
    }
    /**
    * @returns {any}
    */
    getCredentialType() {
        const ret = wasm.vclaim_getCredentialType(this.ptr);
        return takeObject(ret);
    }
    /**
    * @param {any} types
    * @returns {VClaim}
    */
    setCredentialType(types) {
        const ptr = this.ptr;
        this.ptr = 0;
        const ret = wasm.vclaim_setCredentialType(ptr, addHeapObject(types));
        return VClaim.__wrap(ret);
    }
    /**
    * @returns {any}
    */
    getCredentialSubject() {
        const ret = wasm.vclaim_getCredentialSubject(this.ptr);
        return takeObject(ret);
    }
    /**
    * @param {any} cs
    * @returns {VClaim}
    */
    setCredentialSubject(cs) {
        const ptr = this.ptr;
        this.ptr = 0;
        const ret = wasm.vclaim_setCredentialSubject(ptr, addHeapObject(cs));
        return VClaim.__wrap(ret);
    }
    /**
    * @returns {any}
    */
    getId() {
        const ret = wasm.vclaim_getId(this.ptr);
        return takeObject(ret);
    }
    /**
    * @param {string} id
    * @returns {VClaim}
    */
    setId(id) {
        const ptr = this.ptr;
        this.ptr = 0;
        const ret = wasm.vclaim_setId(ptr, passStringToWasm(id), WASM_VECTOR_LEN);
        return VClaim.__wrap(ret);
    }
    /**
    * @returns {any}
    */
    getIssuanceDate() {
        const ret = wasm.vclaim_getIssuanceDate(this.ptr);
        return takeObject(ret);
    }
    /**
    * @param {string} issuance_date
    * @returns {VClaim}
    */
    setIssuanceDate(issuance_date) {
        const ptr = this.ptr;
        this.ptr = 0;
        const ret = wasm.vclaim_setIssuanceDate(ptr, passStringToWasm(issuance_date), WASM_VECTOR_LEN);
        return VClaim.__wrap(ret);
    }
    /**
    * @returns {any}
    */
    getIssuer() {
        const ret = wasm.vclaim_getIssuer(this.ptr);
        return takeObject(ret);
    }
    /**
    * @param {string} issuer
    * @returns {VClaim}
    */
    setIssuer(issuer) {
        const ptr = this.ptr;
        this.ptr = 0;
        const ret = wasm.vclaim_setIssuer(ptr, passStringToWasm(issuer), WASM_VECTOR_LEN);
        return VClaim.__wrap(ret);
    }
}
/**
*
* # WebAssembly gateway for Verifiable Credentials
*/
export class VCredential {

    static __wrap(ptr) {
        const obj = Object.create(VCredential.prototype);
        obj.ptr = ptr;

        return obj;
    }

    free() {
        const ptr = this.ptr;
        this.ptr = 0;

        wasm.__wbg_vcredential_free(ptr);
    }
    /**
    * # Web assembly Constructor
    * `VCredential`\'s constructor will create an empty `VCredential` object. An empty object
    * is not a valid `VCredential`.
    * @param {string} id
    * @returns {VCredential}
    */
    constructor(id) {
        const ret = wasm.vcredential_new(passStringToWasm(id), WASM_VECTOR_LEN);
        return VCredential.__wrap(ret);
    }
    /**
    * # WebAssembly Constructor `formJSON`
    * `VCredential`\'s constructor will create a `VCredential` object from input. Input must be a
    * JSON object with all properties defined. If no value is wanted for certain property, must
    * be empty. This is also true for sub properties (properties of properties).
    * @param {any} json
    * @returns {VCredential}
    */
    static fromJSON(json) {
        const ret = wasm.vcredential_fromJSON(addHeapObject(json));
        return VCredential.__wrap(ret);
    }
    /**
    * # Set context
    * By default `context` will be set to
    * ```json
    * {
        *     context: [
        *         \"https://www.w3.org/2018/credentials/v1\",
        *         \"https://www.w3.org/2018/credentials/examples/v1\
        *     ]
        * }
        * ```
        * To overwrite default `context`s use method `setContext()` can be used.
        * ```javascript
        * import { VCredential } from \'caelum_vcdm\';
        * let vc = new VCredential(\"my_id\")
        *     .setContext(\"new_context\");
        * ```
        * @param {string} c
        * @returns {VCredential}
        */
        setContext(c) {
            const ptr = this.ptr;
            this.ptr = 0;
            const ret = wasm.vcredential_setContext(ptr, passStringToWasm(c), WASM_VECTOR_LEN);
            return VCredential.__wrap(ret);
        }
        /**
        * # Set Type
        * By default `type` will be set to
        * ```json
        * {
            *     type: [
            *         \"VerifiableCredential\",
            *         \"PersonalInformation\
            *     ]
            * }
            * ```
            * To overwrite default `type`s use method `setType()` can be used.
            * ```javascript
            * import { VCredential } from \'caelum_vcdm\';
            * let vc = new VCredential(\"my_id\")
            *     .setType(\"new_type\");
            * ```
            * @param {string} t
            * @returns {VCredential}
            */
            setType(t) {
                const ptr = this.ptr;
                this.ptr = 0;
                const ret = wasm.vcredential_setType(ptr, passStringToWasm(t), WASM_VECTOR_LEN);
                return VCredential.__wrap(ret);
            }
            /**
            * # Set Id
            * By default `id` will be set by the constructor.
            * To overwrite default `id` use method `setId()` can be used.
            * ```javascript
            * import { VCredential } from \'caelum_vcdm\';
            * let vc = new VCredential(\"my_id\")
            *     .setId(\"my_id\");
            * ```
            * @param {string} id
            * @returns {VCredential}
            */
            setId(id) {
                const ptr = this.ptr;
                this.ptr = 0;
                const ret = wasm.vcredential_setId(ptr, passStringToWasm(id), WASM_VECTOR_LEN);
                return VCredential.__wrap(ret);
            }
            /**
            * # Set Issuer
            * By default `issuer` will be set to empty.
            * To overwrite default `issuer` use method `setIssuer()` can be used.
            * ```javascript
            * import { VCredential } from \'caelum_vcdm\';
            * let vc = new VCredential(\"my_id\")
            *     .setIssuer(\"my_issuer\");
            * ```
            * @param {string} i
            * @returns {VCredential}
            */
            setIssuer(i) {
                const ptr = this.ptr;
                this.ptr = 0;
                const ret = wasm.vcredential_setIssuer(ptr, passStringToWasm(i), WASM_VECTOR_LEN);
                return VCredential.__wrap(ret);
            }
            /**
            * # Set Claim
            * By default `claims` will be set to empty array.
            * To overwrite default `claims` use method `setClaims()` can be used.
            * ```javascript
            * import { VCredential } from \'caelum_vcdm\';
            * ```
            * @param {any} c
            * @returns {VCredential}
            */
            setClaim(c) {
                const ptr = this.ptr;
                this.ptr = 0;
                const ret = wasm.vcredential_setClaim(ptr, addHeapObject(c));
                return VCredential.__wrap(ret);
            }
            /**
            * # Set Proof
            * By default `proof` will be set to empty.
            * To overwrite default `proof` use method `setProof()` can be used.
            * ```javascript
            * import { VCredential } from \'caelum_vcdm\';
            * let vc = new VCredential(\"my_id\")
            *     .setProof({
                *        \"type\": \"RsaSignature2018\",
                *        \"created\": \"2018-06-17T10:03:48Z\",
                *        \"verificationMethod\": \"did:example:ebfeb1276e12ec21f712ebc6f1c#k1\",
                *        \"signatureValue\": \"pY9...Cky6Ed = \
                *    });
                * ```
                * @param {any} p
                * @returns {VCredential}
                */
                setProof(p) {
                    const ptr = this.ptr;
                    this.ptr = 0;
                    const ret = wasm.vcredential_setProof(ptr, addHeapObject(p));
                    return VCredential.__wrap(ret);
                }
                /**
                * # Set NonRevocationProof
                * By default `nonRevocationProof` will be set to empty.
                * To overwrite default `nonRevocationProof` use method `setNonRevocationProof()` can be used.
                * ```javascript
                * import { VCredential } from \'caelum_vcdm\';
                * ```
                * @param {any} p
                * @returns {VCredential}
                */
                setNonRevocationProof(p) {
                    const ptr = this.ptr;
                    this.ptr = 0;
                    const ret = wasm.vcredential_setNonRevocationProof(ptr, addHeapObject(p));
                    return VCredential.__wrap(ret);
                }
                /**
                * # Add context
                * By default `context` will be set to
                * ```json
                * {
                    *     context: [
                    *         \"https://www.w3.org/2018/credentials/v1\",
                    *         \"https://www.w3.org/2018/credentials/examples/v1\
                    *     ]
                    * }
                    * ```
                    * To **add** to the default `context` array use method `addContext()` can be used.
                    * ```javascript
                    * import { VCredential } from \'caelum_vcdm\';
                    * let vc = new VCredential(\"my_id\")
                    *     .addContext(\"another_context\");
                    * ```
                    * @param {string} c
                    * @returns {VCredential}
                    */
                    addContext(c) {
                        const ptr = this.ptr;
                        this.ptr = 0;
                        const ret = wasm.vcredential_addContext(ptr, passStringToWasm(c), WASM_VECTOR_LEN);
                        return VCredential.__wrap(ret);
                    }
                    /**
                    * # Add Claim
                    * To **add** to the default `claims` array use method `addClaim()` can be used.
                    * ```javascript
                    * import { VCredential } from \'caelum_vcdm\';
                    * ```
                    * @param {any} c
                    * @returns {VCredential}
                    */
                    addClaim(c) {
                        const ptr = this.ptr;
                        this.ptr = 0;
                        const ret = wasm.vcredential_addClaim(ptr, addHeapObject(c));
                        return VCredential.__wrap(ret);
                    }
                    /**
                    * # Add Proof
                    * By default `proof` will be set to an empty `Vec::new()`.
                    * To overwrite default `proof` use method `addProof()` can be used.
                    * ```javascript
                    * import { VCredential } from \'caelum_vcdm\';
                    * let vc = new VCredential(\"my_id\")
                    *     .addProof({
                        *        \"type\": \"RsaSignature2018\",
                        *        \"created\": \"2018-06-17T10:03:48Z\",
                        *        \"verificationMethod\": \"did:example:ebfeb1276e12ec21f712ebc6f1c#k1\",
                        *        \"signatureValue\": \"pY9...Cky6Ed = \
                        *    });
                        * ```
                        * @param {any} p
                        * @returns {VCredential}
                        */
                        addProof(p) {
                            const ptr = this.ptr;
                            this.ptr = 0;
                            const ret = wasm.vcredential_addProof(ptr, addHeapObject(p));
                            return VCredential.__wrap(ret);
                        }
                        /**
                        * # Add NonRevocationProof
                        * By default `monRevocationProof` will be set to an empty `Vec::new()`.
                        * To overwrite default `nonRevocationProof` use method `addNonRevocationProof()` can be used.
                        * ```javascript
                        * import { VCredential } from \'caelum_vcdm\';
                        * ```
                        * @param {any} p
                        * @returns {VCredential}
                        */
                        addNonRevocationProof(p) {
                            const ptr = this.ptr;
                            this.ptr = 0;
                            const ret = wasm.vcredential_addNonRevocationProof(ptr, addHeapObject(p));
                            return VCredential.__wrap(ret);
                        }
                        /**
                        * # Add Type
                        * By default `type` will be set to:
                        * ```json
                        * {
                            *     type: [
                            *         \"VerifiableCredential\",
                            *         \"PersonalInformation\
                            *     ]
                            * }
                            * ```
                            * Once a property `type` is set, one may want to add to the array. To do so use method
                            * `addType()`.
                            * ```javascript
                            * import { VCredential } from \'caelum_vcdm\';
                            * let vc = new VCredential(\"my_id\")
                            *     .addType(\"new_type\");
                            * ```
                            * @param {string} t
                            * @returns {VCredential}
                            */
                            addType(t) {
                                const ptr = this.ptr;
                                this.ptr = 0;
                                const ret = wasm.vcredential_addType(ptr, passStringToWasm(t), WASM_VECTOR_LEN);
                                return VCredential.__wrap(ret);
                            }
                            /**
                            * # Get Context
                            * ```javascript
                            * import { VCredential } from \'caelum_vcdm\';
                            * let vc = new VCredential(\"my_id\")
                            * console.log(vc.getContext())
                            * ```
                            * The code above will print in console the following:
                            * ```json
                            * {
                                *     context: [
                                *         \"https://www.w3.org/2018/credentials/v1\",
                                *         \"https://www.w3.org/2018/credentials/examples/v1\
                                *     ]
                                * }
                                * ```
                                * @returns {any}
                                */
                                getContext() {
                                    const ret = wasm.vcredential_getContext(this.ptr);
                                    return takeObject(ret);
                                }
                                /**
                                * # Get Type
                                * ```javascript
                                * import { VCredential } from \'caelum_vcdm\';
                                * let vc = new VCredential(\"my_id\")
                                *     .setType({id: \"my_id\", type: \"my_type\"})
                                * console.log(vc.getType())
                                * ```
                                * The code above will print in console the following:
                                * ```json
                                * {
                                    *     id: \"my_id\",
                                    *     type: \"my_type\
                                    * }
                                    * ```
                                    * @returns {any}
                                    */
                                    getType() {
                                        const ret = wasm.vcredential_getType(this.ptr);
                                        return takeObject(ret);
                                    }
                                    /**
                                    * # Get Id
                                    * ```javascript
                                    * import { VCredential } from \'caelum_vcdm\';
                                    * let vc = new VCredential(\"my_id\");
                                    * console.log(vc.getId())
                                    * ```
                                    * The code above will print in console the following:
                                    * ```json
                                    * id: \"my_id\
                                    * ```
                                    * @returns {any}
                                    */
                                    getId() {
                                        const ret = wasm.vcredential_getId(this.ptr);
                                        return takeObject(ret);
                                    }
                                    /**
                                    * # Get Issuer
                                    * ```javascript
                                    * import { VCredential } from \'caelum_vcdm\';
                                    * let vc = new VCredential(\"my_id\")
                                    *     .setIssuanceDate(\"2018-06-17T10:03:48Z\"})
                                    * console.log(vc.getIssuanceDate())
                                    * ```
                                    * The code above will print in console the following:
                                    * ```json
                                    * \"2018-06-17T10:03:48Z\
                                    * ```
                                    * @returns {any}
                                    */
                                    getIssuer() {
                                        const ret = wasm.vcredential_getIssuer(this.ptr);
                                        return takeObject(ret);
                                    }
                                    /**
                                    * # Get Claims
                                    * ```javascript
                                    * import { VCredential } from \'caelum_vcdm\';
                                    * let vc = new VCredential(\"my_id\")
                                    *     .setIssuanceDate(\"2018-06-17T10:03:48Z\"})
                                    * console.log(vc.getIssuanceDate())
                                    * ```
                                    * The code above will print in console the following:
                                    * ```json
                                    * \"2018-06-17T10:03:48Z\
                                    * ```
                                    * @returns {any}
                                    */
                                    getClaims() {
                                        const ret = wasm.vcredential_getClaims(this.ptr);
                                        return takeObject(ret);
                                    }
                                    /**
                                    * # Get Proof
                                    * ```javascript
                                    * import { VCredential } from \'caelum_vcdm\';
                                    * let vc = new VCredential(\"my_id\")
                                    *     .setProof({
                                        *        \"type\": \"RsaSignature2018\",
                                        *        \"created\": \"2018-06-17T10:03:48Z\",
                                        *        \"verificationMethod\": \"did:example:ebfeb1276e12ec21f712ebc6f1c#k1\",
                                        *        \"signatureValue\": \"pY9...Cky6Ed = \
                                        *     })
                                        * console.log(vc.getProof())
                                        * ```
                                        * The code above will print in console the following:
                                        * ```json
                                        * [{
                                            *     \"type\": \"RsaSignature2018\",
                                            *     \"created\": \"2018-06-17T10:03:48Z\",
                                            *     \"verificationMethod\": \"did:example:ebfeb1276e12ec21f712ebc6f1c#k1\",
                                            *     \"signatureValue\": \"pY9...Cky6Ed = \
                                            * }]
                                            * ```
                                            * @returns {any}
                                            */
                                            getProof() {
                                                const ret = wasm.vcredential_getProof(this.ptr);
                                                return takeObject(ret);
                                            }
                                            /**
                                            * # Get NonRevocationProof
                                            * ```javascript
                                            * import { VCredential } from \'caelum_vcdm\';
                                            * ```json
                                            * [{
                                                *     \"type\": \"RsaSignature2018\",
                                                *     \"created\": \"2018-06-17T10:03:48Z\",
                                                *     \"verificationMethod\": \"did:example:ebfeb1276e12ec21f712ebc6f1c#k1\",
                                                *     \"signatureValue\": \"pY9...Cky6Ed = \
                                                * }]
                                                * ```
                                                * @returns {any}
                                                */
                                                getNonRevocationProof() {
                                                    const ret = wasm.vcredential_getNonRevocationProof(this.ptr);
                                                    return takeObject(ret);
                                                }
                                                /**
                                                * # Jsonify Verifiable Credential
                                                * `toJSON` method will output the current `VCredential` as a `JsValue` (json object).
                                                * @returns {any}
                                                */
                                                toJSON() {
                                                    const ret = wasm.vcredential_toJSON(this.ptr);
                                                    return takeObject(ret);
                                                }
                                            }
                                            /**
                                            *
                                            * # WebAssembly gateway for ZenroomProofs
                                            */
                                            export class VNonRevocationProof {

                                                static __wrap(ptr) {
                                                    const obj = Object.create(VNonRevocationProof.prototype);
                                                    obj.ptr = ptr;

                                                    return obj;
                                                }

                                                free() {
                                                    const ptr = this.ptr;
                                                    this.ptr = 0;

                                                    wasm.__wbg_vnonrevocationproof_free(ptr);
                                                }
                                                /**
                                                * @returns {VNonRevocationProof}
                                                */
                                                constructor() {
                                                    const ret = wasm.vnonrevocationproof_new();
                                                    return VNonRevocationProof.__wrap(ret);
                                                }
                                                /**
                                                * @param {any} json
                                                * @returns {VNonRevocationProof}
                                                */
                                                static fromJSON(json) {
                                                    const ret = wasm.vnonrevocationproof_fromJSON(addHeapObject(json));
                                                    return VNonRevocationProof.__wrap(ret);
                                                }
                                                /**
                                                * @returns {any}
                                                */
                                                toJSON() {
                                                    const ret = wasm.vnonrevocationproof_toJSON(this.ptr);
                                                    return takeObject(ret);
                                                }
                                            }
                                            /**
                                            *
                                            * # Webassembly gateway for Verifiable Presentation
                                            */
                                            export class VPresentation {

                                                static __wrap(ptr) {
                                                    const obj = Object.create(VPresentation.prototype);
                                                    obj.ptr = ptr;

                                                    return obj;
                                                }

                                                free() {
                                                    const ptr = this.ptr;
                                                    this.ptr = 0;

                                                    wasm.__wbg_vpresentation_free(ptr);
                                                }
                                                /**
                                                * # Web assembly Constructor
                                                * `VPresentation`\'s constructor will create an empty `VPresentation` object. An empty object
                                                * is not a valid `VPresentation`.
                                                * ```javascript
                                                * import { VPresentation } from \'caelum_vcdm\';
                                                * let vp = new VPresentation(\"my_id\");
                                                * ```
                                                * @param {string} id
                                                * @returns {VPresentation}
                                                */
                                                constructor(id) {
                                                    const ret = wasm.vpresentation_new(passStringToWasm(id), WASM_VECTOR_LEN);
                                                    return VPresentation.__wrap(ret);
                                                }
                                                /**
                                                * # WebAssembly Constructor `formJSON`
                                                * `VPresentation`\'s constructor will create a `VPresentation` object from input. Input must
                                                * be a JSON object with all properties defined. If no value is wanted for certain property,
                                                * must be empty. This is also true for sub properties (properties of properties).
                                                * ```javascript
                                                * import { VPresentation } from \'caelum_vcdm\';
                                                * let vp_json = {
                                                    *    \"@context\": [
                                                    *        \"https://www.w3.org/2018/credentials/v1\",
                                                    *        \"https://www.w3.org/2018/credentials/examples/v1\
                                                    *    ],
                                                    *    \"id\": \"did:example:ebfeb1276e12ec21f712ebc6f1c\",
                                                    *    \"type\": [
                                                    *        \"VerifiableCredential\",
                                                    *        \"PersonalInformation\
                                                    *    ],
                                                    *    \"verifiableCredential\": [
                                                    *        {
                                                        *            \"@context\": [
                                                        *                \"https://www.w3.org/2018/credentials/v1\",
                                                        *                \"https://www.w3.org/2018/credentials/examples/v1\
                                                        *            ],
                                                        *            \"id\": \"http://example.com/credentials/4643\",
                                                        *            \"type\": [
                                                        *                \"VerifiableCredential\",
                                                        *                \"PersonalInformation\
                                                        *            ],
                                                        *            \"issuer\": \"did:example:ebfeb1276e12ec21f712ebc6f1c\",
                                                        *            \"issuanceDate\": \"2010-01-01T19:73:24Z\",
                                                        *            \"credentialStatus\": {
                                                            *                \"id\": \"StatusID\",
                                                            *                \"type\": \"available\
                                                            *            },
                                                            *            \"credentialSubject\": [{
                                                                *                \"type\": \"did:example:abfab3f512ebc6c1c22de17ec77\",
                                                                *                \"name\": \"Mr John Doe\",
                                                                *                \"mnumber\": \"77373737373A\",
                                                                *                \"address\": \"10 Some Street, Anytown, ThisLocal, Country X\",
                                                                *                \"birthDate\": \"1982-02-02-00T00:00Z\
                                                                *            }],
                                                                *            \"proof\": {
                                                                    *                \"type\": \"RsaSignature2018\",
                                                                    *                \"created\": \"2018-06-17T10:03:48Z\",
                                                                    *                \"verificationMethod\": \"did:example:ebfeb1276e12ec21f712ebc6f1c#k1\",
                                                                    *                \"signatureValue\": \"pY9...Cky6Ed = \
                                                                    *            }
                                                                    *        }
                                                                    *    ],
                                                                    *    \"proof\": {
                                                                        *        \"type\": \"RsaSignature2018\",
                                                                        *        \"created\": \"2018-06-17T10:03:48Z\",
                                                                        *        \"verificationMethod\": \"did:example:ebfeb1276e12ec21f712ebc6f1c#k1\",
                                                                        *        \"signatureValue\": \"pY9...Cky6Ed = \
                                                                        *    }
                                                                        * };
                                                                        * let ver_pres = VPresentation.fromJSON(vp_json);
                                                                        * ```
                                                                        * @param {any} json
                                                                        * @returns {VPresentation}
                                                                        */
                                                                        static fromJSON(json) {
                                                                            const ret = wasm.vpresentation_fromJSON(addHeapObject(json));
                                                                            return VPresentation.__wrap(ret);
                                                                        }
                                                                        /**
                                                                        * # Set context
                                                                        * By default `context` will be set to
                                                                        * ```json
                                                                        * {
                                                                            *     context: [
                                                                            *         \"https://www.w3.org/2018/credentials/v1\",
                                                                            *         \"https://www.w3.org/2018/credentials/examples/v1\
                                                                            *     ]
                                                                            * }
                                                                            * ```
                                                                            * To overwrite default `context`s use method `setContext()` can be used.
                                                                            * ```javascript
                                                                            * import { VPresentation } from \'caelum_vcdm\';
                                                                            * let vp = new VPresentation(\"my_id\")
                                                                            *     .setContext(\"new_context\");
                                                                            * ```
                                                                            * @param {string} c
                                                                            * @returns {VPresentation}
                                                                            */
                                                                            setContext(c) {
                                                                                const ptr = this.ptr;
                                                                                this.ptr = 0;
                                                                                const ret = wasm.vpresentation_setContext(ptr, passStringToWasm(c), WASM_VECTOR_LEN);
                                                                                return VPresentation.__wrap(ret);
                                                                            }
                                                                            /**
                                                                            * # Set Id
                                                                            * By default `id` will be set by the constructor.
                                                                            * To overwrite default `id` use method `setId()` can be used.
                                                                            * ```javascript
                                                                            * import { VPresentation } from \'caelum_vcdm\';
                                                                            * let vp = new VPresentation(\"my_id\")
                                                                            *     .setId(\"my_id\");
                                                                            * ```
                                                                            * @param {string} id
                                                                            * @returns {VPresentation}
                                                                            */
                                                                            setId(id) {
                                                                                const ptr = this.ptr;
                                                                                this.ptr = 0;
                                                                                const ret = wasm.vpresentation_setId(ptr, passStringToWasm(id), WASM_VECTOR_LEN);
                                                                                return VPresentation.__wrap(ret);
                                                                            }
                                                                            /**
                                                                            * # Set Type
                                                                            * By default `type` will be set to
                                                                            * ```json
                                                                            * {
                                                                                *     type: [
                                                                                *         \"VerifiableCredential\",
                                                                                *         \"PersonalInformation\
                                                                                *     ]
                                                                                * }
                                                                                * ```
                                                                                * To overwrite default `type`s use method `setType()` can be used.
                                                                                * ```javascript
                                                                                * import { VPresentation } from \'caelum_vcdm\';
                                                                                * let vp = new VPresentation(\"my_id\")
                                                                                *     .setType(\"new_type\");
                                                                                * ```
                                                                                * @param {string} t
                                                                                * @returns {VPresentation}
                                                                                */
                                                                                setType(t) {
                                                                                    const ptr = this.ptr;
                                                                                    this.ptr = 0;
                                                                                    const ret = wasm.vpresentation_setType(ptr, passStringToWasm(t), WASM_VECTOR_LEN);
                                                                                    return VPresentation.__wrap(ret);
                                                                                }
                                                                                /**
                                                                                * # Set Verifiable Credential
                                                                                * By default `verifiableCredential` will be an empty vector `Vec::new()` representing
                                                                                * `Vec<VerifiableCredential>. A array of json objects representing a `VCredential`.
                                                                                * ```javascript
                                                                                * import { VPresentation } from \'caelum_vcdm\';
                                                                                * let vp = new VPresentation(\"my_id\")
                                                                                *     // Variable `vc` represents a `VCredential` object
                                                                                *     .addVerifiableCredential(vp.toJSON())
                                                                                * ```
                                                                                * Equivalent to
                                                                                * ```javascript
                                                                                * import { VPresentation } from \'caelum_vcdm\';
                                                                                * let vp = new VPresentation(\"my_id\")
                                                                                *     .addVerifiableCredential({
                                                                                    *         \"@context\": [
                                                                                    *             \"https://www.w3.org/2018/credentials/v1\",
                                                                                    *             \"https://www.w3.org/2018/credentials/examples/v1\
                                                                                    *         ],
                                                                                    *         \"id\": \"http://example.com/credentials/4643\",
                                                                                    *         \"type\": [
                                                                                    *             \"VerifiableCredential\",
                                                                                    *             \"PersonalInformation\
                                                                                    *         ],
                                                                                    *         \"issuer\": \"did:example:ebfeb1276e12ec21f712ebc6f1c\",
                                                                                    *         \"issuanceDate\": \"2010-01-01T19:73:24Z\",
                                                                                    *         \"credentialStatus\": {
                                                                                        *             \"id\": \"StatusID\",
                                                                                        *             \"type\": \"available\
                                                                                        *         },
                                                                                        *         \"credentialSubject\": [{
                                                                                            *             \"type\": \"did:example:abfab3f512ebc6c1c22de17ec77\",
                                                                                            *             \"name\": \"Mr John Doe\",
                                                                                            *             \"mnumber\": \"77373737373A\",
                                                                                            *             \"address\": \"10 Some Street, Anytown, ThisLocal, Country X\",
                                                                                            *             \"birthDate\": \"1982-02-02-00T00:00Z\
                                                                                            *         }],
                                                                                            *         \"proof\": {
                                                                                                *             \"type\": \"RsaSignature2018\",
                                                                                                *             \"created\": \"2018-06-17T10:03:48Z\",
                                                                                                *             \"verificationMethod\": \"did:example:ebfeb1276e12ec21f712ebc6f1c#k1\",
                                                                                                *             \"signatureValue\": \"pY9...Cky6Ed = \
                                                                                                *         }
                                                                                                *     });
                                                                                                * ```
                                                                                                * @param {any} json
                                                                                                * @returns {VPresentation}
                                                                                                */
                                                                                                setVerifiableCredential(json) {
                                                                                                    const ptr = this.ptr;
                                                                                                    this.ptr = 0;
                                                                                                    const ret = wasm.vpresentation_setVerifiableCredential(ptr, addHeapObject(json));
                                                                                                    return VPresentation.__wrap(ret);
                                                                                                }
                                                                                                /**
                                                                                                * # Set Proof
                                                                                                * /// By default `proof` will be set to empty.
                                                                                                * To overwrite default `proof` use method `setProof()` can be used.
                                                                                                * ```javascript
                                                                                                * import { VPresentation } from \'caelum_vcdm\';
                                                                                                * let vp = new VPresentation(\"my_id\")
                                                                                                *     .setProof({
                                                                                                    *         \"type\": \"RsaSignature2018\",
                                                                                                    *         \"created\": \"2018-06-17T10:03:48Z\",
                                                                                                    *         \"verificationMethod\": \"did:example:ebfeb1276e12ec21f712ebc6f1c#k1\",
                                                                                                    *         \"signatureValue\": \"pY9...Cky6Ed = \
                                                                                                    *     });
                                                                                                    * ```
                                                                                                    * @param {any} p
                                                                                                    * @returns {VPresentation}
                                                                                                    */
                                                                                                    setProof(p) {
                                                                                                        const ptr = this.ptr;
                                                                                                        this.ptr = 0;
                                                                                                        const ret = wasm.vpresentation_setProof(ptr, addHeapObject(p));
                                                                                                        return VPresentation.__wrap(ret);
                                                                                                    }
                                                                                                    /**
                                                                                                    * # Add context
                                                                                                    * By default `context` will be set to
                                                                                                    * ```json
                                                                                                    * {
                                                                                                        *     context: [
                                                                                                        *         \"https://www.w3.org/2018/credentials/v1\",
                                                                                                        *         \"https://www.w3.org/2018/credentials/examples/v1\
                                                                                                        *     ]
                                                                                                        * }
                                                                                                        * ```
                                                                                                        * To **add** to the default `context` array use method `addContext()` can be used.
                                                                                                        * ```javascript
                                                                                                        * import { VPresentation } from \'caelum_vcdm\';
                                                                                                        * let vp = new VPresentation(\"my_id\")
                                                                                                        *     .addContext(\"another_context\");
                                                                                                        * ```
                                                                                                        * @param {string} c
                                                                                                        * @returns {VPresentation}
                                                                                                        */
                                                                                                        addContext(c) {
                                                                                                            const ptr = this.ptr;
                                                                                                            this.ptr = 0;
                                                                                                            const ret = wasm.vpresentation_addContext(ptr, passStringToWasm(c), WASM_VECTOR_LEN);
                                                                                                            return VPresentation.__wrap(ret);
                                                                                                        }
                                                                                                        /**
                                                                                                        * # Add Type
                                                                                                        * By default `type` will be set to:
                                                                                                        * ```json
                                                                                                        * {
                                                                                                            *     type: [
                                                                                                            *         \"VerifiableCredential\",
                                                                                                            *         \"PersonalInformation\
                                                                                                            *     ]
                                                                                                            * }
                                                                                                            * ```
                                                                                                            * Once a property `type` is set, one may want to add to the array. To do so use method
                                                                                                            * `addType()`.
                                                                                                            * ```javascript
                                                                                                            * import { VPresentation } from \'caelum_vcdm\';
                                                                                                            * let vp = new VPresentation(\"my_id\")
                                                                                                            *     .addType(\"new_type\");
                                                                                                            * ```
                                                                                                            * @param {string} t
                                                                                                            * @returns {VPresentation}
                                                                                                            */
                                                                                                            addType(t) {
                                                                                                                const ptr = this.ptr;
                                                                                                                this.ptr = 0;
                                                                                                                const ret = wasm.vpresentation_addType(ptr, passStringToWasm(t), WASM_VECTOR_LEN);
                                                                                                                return VPresentation.__wrap(ret);
                                                                                                            }
                                                                                                            /**
                                                                                                            * # Add Verifiable Credential
                                                                                                            * By default `verifiableCredential` create an empty vector. In order to add/push more
                                                                                                            * `verifiableCredential`s to the array use method `addVerifiableCredential()`.
                                                                                                            * ```javascript
                                                                                                            * import { VPresentation } from \'caelum_vcdm\';
                                                                                                            * let vp = new VPresentation(\"my_id\")
                                                                                                            *     .addVerifiableCredential({
                                                                                                                *         \"@context\": [
                                                                                                                *             \"https://www.w3.org/2018/credentials/v1\",
                                                                                                                *             \"https://www.w3.org/2018/credentials/examples/v1\
                                                                                                                *         ],
                                                                                                                *         \"id\": \"http://example.com/credentials/4643\",
                                                                                                                *         \"type\": [
                                                                                                                *             \"VerifiableCredential\",
                                                                                                                *             \"PersonalInformation\
                                                                                                                *         ],
                                                                                                                *         \"issuer\": \"did:example:ebfeb1276e12ec21f712ebc6f1c\",
                                                                                                                *         \"issuanceDate\": \"2010-01-01T19:73:24Z\",
                                                                                                                *         \"credentialStatus\": {
                                                                                                                    *             \"id\": \"StatusID\",
                                                                                                                    *             \"type\": \"available\
                                                                                                                    *         },
                                                                                                                    *         \"credentialSubject\": [{
                                                                                                                        *             \"type\": \"did:example:abfab3f512ebc6c1c22de17ec77\",
                                                                                                                        *             \"name\": \"Mr John Doe\",
                                                                                                                        *             \"mnumber\": \"77373737373A\",
                                                                                                                        *             \"address\": \"10 Some Street, Anytown, ThisLocal, Country X\",
                                                                                                                        *             \"birthDate\": \"1982-02-02-00T00:00Z\
                                                                                                                        *         }],
                                                                                                                        *         \"proof\": {
                                                                                                                            *             \"type\": \"RsaSignature2018\",
                                                                                                                            *             \"created\": \"2018-06-17T10:03:48Z\",
                                                                                                                            *             \"verificationMethod\": \"did:example:ebfeb1276e12ec21f712ebc6f1c#k1\",
                                                                                                                            *             \"signatureValue\": \"pY9...Cky6Ed = \
                                                                                                                            *         }
                                                                                                                            *     });
                                                                                                                            * ```
                                                                                                                            * @param {any} vc
                                                                                                                            * @returns {VPresentation}
                                                                                                                            */
                                                                                                                            addVerifiableCredential(vc) {
                                                                                                                                const ptr = this.ptr;
                                                                                                                                this.ptr = 0;
                                                                                                                                const ret = wasm.vpresentation_addVerifiableCredential(ptr, addHeapObject(vc));
                                                                                                                                return VPresentation.__wrap(ret);
                                                                                                                            }
                                                                                                                            /**
                                                                                                                            * # Get Context
                                                                                                                            * ```javascript
                                                                                                                            * import { VPresentation } from \'caelum_vcdm\';
                                                                                                                            * let vp = new VPresentation(\"my_id\")
                                                                                                                            * console.log(vp.getContext())
                                                                                                                            * ```
                                                                                                                            * The code above will print in console the following:
                                                                                                                            * ```json
                                                                                                                            * {
                                                                                                                                *     context: [
                                                                                                                                *         \"https://www.w3.org/2018/credentials/v1\",
                                                                                                                                *         \"https://www.w3.org/2018/credentials/examples/v1\
                                                                                                                                *     ]
                                                                                                                                * }
                                                                                                                                * ```
                                                                                                                                * @returns {any}
                                                                                                                                */
                                                                                                                                getContext() {
                                                                                                                                    const ret = wasm.vpresentation_getContext(this.ptr);
                                                                                                                                    return takeObject(ret);
                                                                                                                                }
                                                                                                                                /**
                                                                                                                                * # Get Verifiable Credential
                                                                                                                                * ```javascript
                                                                                                                                * import { VPresentation } from \'caelum_vcdm\';
                                                                                                                                * let vp = new VPresentation(\"my_id\")
                                                                                                                                *     addVerifiableCredential(serde_json::from_str(r#\"{
                                                                                                                                    *         \"@context\": [
                                                                                                                                    *             \"https://www.w3.org/2018/credentials/v1\",
                                                                                                                                    *             \"https://www.w3.org/2018/credentials/examples/v1\
                                                                                                                                    *         ],
                                                                                                                                    *         \"id\": \"http://example.com/credentials/4643\",
                                                                                                                                    *         \"type\": [
                                                                                                                                    *             \"VerifiableCredential\",
                                                                                                                                    *             \"PersonalInformation\
                                                                                                                                    *         ],
                                                                                                                                    *         \"issuer\": \"did:example:ebfeb1276e12ec21f712ebc6f1c\",
                                                                                                                                    *         \"issuanceDate\": \"2010-01-01T19:73:24Z\",
                                                                                                                                    *         \"credentialStatus\": {
                                                                                                                                        *             \"id\": \"StatusID\",
                                                                                                                                        *             \"type\": \"available\
                                                                                                                                        *         },
                                                                                                                                        *         \"credentialSubject\": [{
                                                                                                                                            *             \"type\": \"did:example:abfab3f512ebc6c1c22de17ec77\",
                                                                                                                                            *             \"name\": \"Mr John Doe\",
                                                                                                                                            *             \"mnumber\": \"77373737373A\",
                                                                                                                                            *             \"address\": \"10 Some Street, Anytown, ThisLocal, Country X\",
                                                                                                                                            *             \"birthDate\": \"1982-02-02-00T00:00Z\
                                                                                                                                            *         }],
                                                                                                                                            *         \"proof\": [{
                                                                                                                                                *             \"type\": \"RsaSignature2018\",
                                                                                                                                                *             \"created\": \"2018-06-17T10:03:48Z\",
                                                                                                                                                *             \"verificationMethod\": \"did:example:ebfeb1276e12ec21f712ebc6f1c#k1\",
                                                                                                                                                *             \"signatureValue\": \"pY9...Cky6Ed = \
                                                                                                                                                *         }]
                                                                                                                                                *     }\"#).unwrap());
                                                                                                                                                * console.log(vp.getVerifialbleCredential());
                                                                                                                                                * ```
                                                                                                                                                * The code above will print in console the following:
                                                                                                                                                * ```json
                                                                                                                                                * {
                                                                                                                                                    *         \"@context\": [
                                                                                                                                                    *             \"https://www.w3.org/2018/credentials/v1\",
                                                                                                                                                    *             \"https://www.w3.org/2018/credentials/examples/v1\
                                                                                                                                                    *         ],
                                                                                                                                                    *         \"id\": \"http://example.com/credentials/4643\",
                                                                                                                                                    *         \"type\": [
                                                                                                                                                    *             \"VerifiableCredential\",
                                                                                                                                                    *             \"PersonalInformation\
                                                                                                                                                    *         ],
                                                                                                                                                    *         \"issuer\": \"did:example:ebfeb1276e12ec21f712ebc6f1c\",
                                                                                                                                                    *         \"issuanceDate\": \"2010-01-01T19:73:24Z\",
                                                                                                                                                    *         \"credentialStatus\": {
                                                                                                                                                        *             \"id\": \"StatusID\",
                                                                                                                                                        *             \"type\": \"available\
                                                                                                                                                        *         },
                                                                                                                                                        *         \"credentialSubject\": [{
                                                                                                                                                            *             \"type\": \"did:example:abfab3f512ebc6c1c22de17ec77\",
                                                                                                                                                            *             \"name\": \"Mr John Doe\",
                                                                                                                                                            *             \"mnumber\": \"77373737373A\",
                                                                                                                                                            *             \"address\": \"10 Some Street, Anytown, ThisLocal, Country X\",
                                                                                                                                                            *             \"birthDate\": \"1982-02-02-00T00:00Z\
                                                                                                                                                            *         }],
                                                                                                                                                            *         \"proof\": {
                                                                                                                                                                *             \"type\": \"RsaSignature2018\",
                                                                                                                                                                *             \"created\": \"2018-06-17T10:03:48Z\",
                                                                                                                                                                *             \"verificationMethod\": \"did:example:ebfeb1276e12ec21f712ebc6f1c#k1\",
                                                                                                                                                                *             \"signatureValue\": \"pY9...Cky6Ed = \
                                                                                                                                                                *         }
                                                                                                                                                                *     }
                                                                                                                                                                * }
                                                                                                                                                                * ```
                                                                                                                                                                * @returns {any}
                                                                                                                                                                */
                                                                                                                                                                getVerifialbleCredential() {
                                                                                                                                                                    const ret = wasm.vpresentation_getVerifialbleCredential(this.ptr);
                                                                                                                                                                    return takeObject(ret);
                                                                                                                                                                }
                                                                                                                                                                /**
                                                                                                                                                                * # Get Type
                                                                                                                                                                * ```javascript
                                                                                                                                                                * import { VPresentation } from \'caelum_vcdm\';
                                                                                                                                                                * let vp = new VPresentation(\"my_id\")
                                                                                                                                                                *     .setType(\"my_type\")
                                                                                                                                                                *     .addType(\"my_type2\")
                                                                                                                                                                * console.log(vp.getType())
                                                                                                                                                                * ```
                                                                                                                                                                * The code above will print in console the following:
                                                                                                                                                                * ```json
                                                                                                                                                                * [\"my_type\", \"my_type2\"]
                                                                                                                                                                * ```
                                                                                                                                                                * @returns {any}
                                                                                                                                                                */
                                                                                                                                                                getType() {
                                                                                                                                                                    const ret = wasm.vpresentation_getType(this.ptr);
                                                                                                                                                                    return takeObject(ret);
                                                                                                                                                                }
                                                                                                                                                                /**
                                                                                                                                                                * # Get Id
                                                                                                                                                                * ```javascript
                                                                                                                                                                * import { VPresentation } from \'caelum_vcdm\';
                                                                                                                                                                * let vp = new VPresentation(\"my_id\");
                                                                                                                                                                * console.log(vp.getId())
                                                                                                                                                                * ```
                                                                                                                                                                * The code above will print in console the following:
                                                                                                                                                                * ```json
                                                                                                                                                                * id: \"my_id\
                                                                                                                                                                * ```
                                                                                                                                                                * @returns {any}
                                                                                                                                                                */
                                                                                                                                                                getId() {
                                                                                                                                                                    const ret = wasm.vpresentation_getId(this.ptr);
                                                                                                                                                                    return takeObject(ret);
                                                                                                                                                                }
                                                                                                                                                                /**
                                                                                                                                                                * # Get Proof
                                                                                                                                                                * ```javascript
                                                                                                                                                                * import { VPresentation } from \'caelum_vcdm\';
                                                                                                                                                                * let vp = new VPresentation(\"my_id\")
                                                                                                                                                                *     .setProof({
                                                                                                                                                                    *        \"type\": \"RsaSignature2018\",
                                                                                                                                                                    *        \"created\": \"2018-06-17T10:03:48Z\",
                                                                                                                                                                    *        \"verificationMethod\": \"did:example:ebfeb1276e12ec21f712ebc6f1c#k1\",
                                                                                                                                                                    *        \"signatureValue\": \"pY9...Cky6Ed = \
                                                                                                                                                                    *     })
                                                                                                                                                                    * console.log(vp.getProof())
                                                                                                                                                                    * ```
                                                                                                                                                                    * The code above will print in console the following:
                                                                                                                                                                    * ```json
                                                                                                                                                                    * {
                                                                                                                                                                        *     \"type\": \"RsaSignature2018\",
                                                                                                                                                                        *     \"created\": \"2018-06-17T10:03:48Z\",
                                                                                                                                                                        *     \"verificationMethod\": \"did:example:ebfeb1276e12ec21f712ebc6f1c#k1\",
                                                                                                                                                                        *     \"signatureValue\": \"pY9...Cky6Ed = \
                                                                                                                                                                        * }
                                                                                                                                                                        * ```
                                                                                                                                                                        * @returns {any}
                                                                                                                                                                        */
                                                                                                                                                                        getProof() {
                                                                                                                                                                            const ret = wasm.vpresentation_getProof(this.ptr);
                                                                                                                                                                            return takeObject(ret);
                                                                                                                                                                        }
                                                                                                                                                                        /**
                                                                                                                                                                        * # Jsonify Verifiable Presentation
                                                                                                                                                                        * `toJSON` method will output the current `VPresentation` as a `JsValue` (json object).
                                                                                                                                                                        * @returns {any}
                                                                                                                                                                        */
                                                                                                                                                                        toJSON() {
                                                                                                                                                                            const ret = wasm.vpresentation_toJSON(this.ptr);
                                                                                                                                                                            return takeObject(ret);
                                                                                                                                                                        }
                                                                                                                                                                    }
                                                                                                                                                                    /**
                                                                                                                                                                    *
                                                                                                                                                                    * # WebAssembly gateway for Proofs
                                                                                                                                                                    */
                                                                                                                                                                    export class VProof {

                                                                                                                                                                        static __wrap(ptr) {
                                                                                                                                                                            const obj = Object.create(VProof.prototype);
                                                                                                                                                                            obj.ptr = ptr;

                                                                                                                                                                            return obj;
                                                                                                                                                                        }

                                                                                                                                                                        free() {
                                                                                                                                                                            const ptr = this.ptr;
                                                                                                                                                                            this.ptr = 0;

                                                                                                                                                                            wasm.__wbg_vproof_free(ptr);
                                                                                                                                                                        }
                                                                                                                                                                        /**
                                                                                                                                                                        * @returns {VProof}
                                                                                                                                                                        */
                                                                                                                                                                        constructor() {
                                                                                                                                                                            const ret = wasm.vproof_new();
                                                                                                                                                                            return VProof.__wrap(ret);
                                                                                                                                                                        }
                                                                                                                                                                        /**
                                                                                                                                                                        * @param {any} json
                                                                                                                                                                        * @returns {VProof}
                                                                                                                                                                        */
                                                                                                                                                                        static fromJSON(json) {
                                                                                                                                                                            const ret = wasm.vproof_fromJSON(addHeapObject(json));
                                                                                                                                                                            return VProof.__wrap(ret);
                                                                                                                                                                        }
                                                                                                                                                                        /**
                                                                                                                                                                        * @param {any} json_claim
                                                                                                                                                                        * @param {Uint8Array} keys
                                                                                                                                                                        * @returns {VProof}
                                                                                                                                                                        */
                                                                                                                                                                        static fromClaimAndKeys(json_claim, keys) {
                                                                                                                                                                            const ret = wasm.vproof_fromClaimAndKeys(addHeapObject(json_claim), passArray8ToWasm(keys), WASM_VECTOR_LEN);
                                                                                                                                                                            return VProof.__wrap(ret);
                                                                                                                                                                        }
                                                                                                                                                                        /**
                                                                                                                                                                        * @returns {any}
                                                                                                                                                                        */
                                                                                                                                                                        toJSON() {
                                                                                                                                                                            const ret = wasm.vproof_toJSON(this.ptr);
                                                                                                                                                                            return takeObject(ret);
                                                                                                                                                                        }
                                                                                                                                                                    }

                                                                                                                                                                    export const __wbindgen_object_drop_ref = function(arg0) {
                                                                                                                                                                        takeObject(arg0);
                                                                                                                                                                    };

                                                                                                                                                                    export const __wbindgen_object_clone_ref = function(arg0) {
                                                                                                                                                                        const ret = getObject(arg0);
                                                                                                                                                                        return addHeapObject(ret);
                                                                                                                                                                    };

                                                                                                                                                                    export const __wbindgen_json_parse = function(arg0, arg1) {
                                                                                                                                                                        const ret = JSON.parse(getStringFromWasm(arg0, arg1));
                                                                                                                                                                        return addHeapObject(ret);
                                                                                                                                                                    };

                                                                                                                                                                    export const __wbindgen_json_serialize = function(arg0, arg1) {
                                                                                                                                                                        const obj = getObject(arg1);
                                                                                                                                                                        const ret = JSON.stringify(obj === undefined ? null : obj);
                                                                                                                                                                        const ret0 = passStringToWasm(ret);
                                                                                                                                                                        const ret1 = WASM_VECTOR_LEN;
                                                                                                                                                                        getInt32Memory()[arg0 / 4 + 0] = ret0;
                                                                                                                                                                        getInt32Memory()[arg0 / 4 + 1] = ret1;
                                                                                                                                                                    };

                                                                                                                                                                    export const __wbindgen_throw = function(arg0, arg1) {
                                                                                                                                                                        throw new Error(getStringFromWasm(arg0, arg1));
                                                                                                                                                                    };

