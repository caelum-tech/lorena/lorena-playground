/* tslint:disable */
/**
*
* # Utils interface
* @param {string} s 
* @returns {string} 
*/
export function create_hash(s: string): string;
/**
* @param {string} s 
* @returns {Uint8Array} 
*/
export function create_u8a_hash(s: string): Uint8Array;
/**
* @param {string} hexa 
* @returns {Uint8Array} 
*/
export function hex_to_u8a(hexa: string): Uint8Array;
/**
* @param {Uint8Array} vec 
* @returns {string} 
*/
export function u8a_to_hex(vec: Uint8Array): string;
/**
* @param {any} claim 
* @param {any} proof 
* @returns {boolean} 
*/
export function verify_claim(claim: any, proof: any): boolean;
/**
*
* # WebAssembly gateway for Claims
*/
export class VClaim {
  free(): void;
/**
* @returns {VClaim} 
*/
  constructor();
/**
* @param {any} json 
* @returns {VClaim} 
*/
  static fromJSON(json: any): VClaim;
/**
* @returns {any} 
*/
  toJSON(): any;
/**
* @param {Uint8Array} keys 
* @returns {string} 
*/
  sign(keys: Uint8Array): string;
/**
* @param {any} proof 
* @returns {boolean} 
*/
  verify(proof: any): boolean;
/**
* @returns {any} 
*/
  getCredentialStatus(): any;
/**
* @param {any} cs 
* @returns {VClaim} 
*/
  setCredentialStatus(cs: any): VClaim;
/**
* @returns {any} 
*/
  getCredentialType(): any;
/**
* @param {any} types 
* @returns {VClaim} 
*/
  setCredentialType(types: any): VClaim;
/**
* @returns {any} 
*/
  getCredentialSubject(): any;
/**
* @param {any} cs 
* @returns {VClaim} 
*/
  setCredentialSubject(cs: any): VClaim;
/**
* @returns {any} 
*/
  getId(): any;
/**
* @param {string} id 
* @returns {VClaim} 
*/
  setId(id: string): VClaim;
/**
* @returns {any} 
*/
  getIssuanceDate(): any;
/**
* @param {string} issuance_date 
* @returns {VClaim} 
*/
  setIssuanceDate(issuance_date: string): VClaim;
/**
* @returns {any} 
*/
  getIssuer(): any;
/**
* @param {string} issuer 
* @returns {VClaim} 
*/
  setIssuer(issuer: string): VClaim;
}
/**
*
* # WebAssembly gateway for Verifiable Credentials
*/
export class VCredential {
  free(): void;
/**
* # Web assembly Constructor
* `VCredential`\'s constructor will create an empty `VCredential` object. An empty object
* is not a valid `VCredential`.
* @param {string} id 
* @returns {VCredential} 
*/
  constructor(id: string);
/**
* # WebAssembly Constructor `formJSON`
* `VCredential`\'s constructor will create a `VCredential` object from input. Input must be a
* JSON object with all properties defined. If no value is wanted for certain property, must
* be empty. This is also true for sub properties (properties of properties).
* @param {any} json 
* @returns {VCredential} 
*/
  static fromJSON(json: any): VCredential;
/**
* # Set context
* By default `context` will be set to
* ```json
* {
*     context: [
*         \"https://www.w3.org/2018/credentials/v1\",
*         \"https://www.w3.org/2018/credentials/examples/v1\
*     ]
* }
* ```
* To overwrite default `context`s use method `setContext()` can be used.
* ```javascript
* import { VCredential } from \'caelum_vcdm\';
* let vc = new VCredential(\"my_id\")
*     .setContext(\"new_context\");
* ```
* @param {string} c 
* @returns {VCredential} 
*/
  setContext(c: string): VCredential;
/**
* # Set Type
* By default `type` will be set to
* ```json
* {
*     type: [
*         \"VerifiableCredential\",
*         \"PersonalInformation\
*     ]
* }
* ```
* To overwrite default `type`s use method `setType()` can be used.
* ```javascript
* import { VCredential } from \'caelum_vcdm\';
* let vc = new VCredential(\"my_id\")
*     .setType(\"new_type\");
* ```
* @param {string} t 
* @returns {VCredential} 
*/
  setType(t: string): VCredential;
/**
* # Set Id
* By default `id` will be set by the constructor.
* To overwrite default `id` use method `setId()` can be used.
* ```javascript
* import { VCredential } from \'caelum_vcdm\';
* let vc = new VCredential(\"my_id\")
*     .setId(\"my_id\");
* ```
* @param {string} id 
* @returns {VCredential} 
*/
  setId(id: string): VCredential;
/**
* # Set Issuer
* By default `issuer` will be set to empty.
* To overwrite default `issuer` use method `setIssuer()` can be used.
* ```javascript
* import { VCredential } from \'caelum_vcdm\';
* let vc = new VCredential(\"my_id\")
*     .setIssuer(\"my_issuer\");
* ```
* @param {string} i 
* @returns {VCredential} 
*/
  setIssuer(i: string): VCredential;
/**
* # Set Claim
* By default `claims` will be set to empty array.
* To overwrite default `claims` use method `setClaims()` can be used.
* ```javascript
* import { VCredential } from \'caelum_vcdm\';
* ```
* @param {any} c 
* @returns {VCredential} 
*/
  setClaim(c: any): VCredential;
/**
* # Set Proof
* By default `proof` will be set to empty.
* To overwrite default `proof` use method `setProof()` can be used.
* ```javascript
* import { VCredential } from \'caelum_vcdm\';
* let vc = new VCredential(\"my_id\")
*     .setProof({
*        \"type\": \"RsaSignature2018\",
*        \"created\": \"2018-06-17T10:03:48Z\",
*        \"verificationMethod\": \"did:example:ebfeb1276e12ec21f712ebc6f1c#k1\",
*        \"signatureValue\": \"pY9...Cky6Ed = \
*    });
* ```
* @param {any} p 
* @returns {VCredential} 
*/
  setProof(p: any): VCredential;
/**
* # Set NonRevocationProof
* By default `nonRevocationProof` will be set to empty.
* To overwrite default `nonRevocationProof` use method `setNonRevocationProof()` can be used.
* ```javascript
* import { VCredential } from \'caelum_vcdm\';
* ```
* @param {any} p 
* @returns {VCredential} 
*/
  setNonRevocationProof(p: any): VCredential;
/**
* # Add context
* By default `context` will be set to
* ```json
* {
*     context: [
*         \"https://www.w3.org/2018/credentials/v1\",
*         \"https://www.w3.org/2018/credentials/examples/v1\
*     ]
* }
* ```
* To **add** to the default `context` array use method `addContext()` can be used.
* ```javascript
* import { VCredential } from \'caelum_vcdm\';
* let vc = new VCredential(\"my_id\")
*     .addContext(\"another_context\");
* ```
* @param {string} c 
* @returns {VCredential} 
*/
  addContext(c: string): VCredential;
/**
* # Add Claim
* To **add** to the default `claims` array use method `addClaim()` can be used.
* ```javascript
* import { VCredential } from \'caelum_vcdm\';
* ```
* @param {any} c 
* @returns {VCredential} 
*/
  addClaim(c: any): VCredential;
/**
* # Add Proof
* By default `proof` will be set to an empty `Vec::new()`.
* To overwrite default `proof` use method `addProof()` can be used.
* ```javascript
* import { VCredential } from \'caelum_vcdm\';
* let vc = new VCredential(\"my_id\")
*     .addProof({
*        \"type\": \"RsaSignature2018\",
*        \"created\": \"2018-06-17T10:03:48Z\",
*        \"verificationMethod\": \"did:example:ebfeb1276e12ec21f712ebc6f1c#k1\",
*        \"signatureValue\": \"pY9...Cky6Ed = \
*    });
* ```
* @param {any} p 
* @returns {VCredential} 
*/
  addProof(p: any): VCredential;
/**
* # Add NonRevocationProof
* By default `monRevocationProof` will be set to an empty `Vec::new()`.
* To overwrite default `nonRevocationProof` use method `addNonRevocationProof()` can be used.
* ```javascript
* import { VCredential } from \'caelum_vcdm\';
* ```
* @param {any} p 
* @returns {VCredential} 
*/
  addNonRevocationProof(p: any): VCredential;
/**
* # Add Type
* By default `type` will be set to:
* ```json
* {
*     type: [
*         \"VerifiableCredential\",
*         \"PersonalInformation\
*     ]
* }
* ```
* Once a property `type` is set, one may want to add to the array. To do so use method
* `addType()`.
* ```javascript
* import { VCredential } from \'caelum_vcdm\';
* let vc = new VCredential(\"my_id\")
*     .addType(\"new_type\");
* ```
* @param {string} t 
* @returns {VCredential} 
*/
  addType(t: string): VCredential;
/**
* # Get Context
* ```javascript
* import { VCredential } from \'caelum_vcdm\';
* let vc = new VCredential(\"my_id\")
* console.log(vc.getContext())
* ```
* The code above will print in console the following:
* ```json
* {
*     context: [
*         \"https://www.w3.org/2018/credentials/v1\",
*         \"https://www.w3.org/2018/credentials/examples/v1\
*     ]
* }
* ```
* @returns {any} 
*/
  getContext(): any;
/**
* # Get Type
* ```javascript
* import { VCredential } from \'caelum_vcdm\';
* let vc = new VCredential(\"my_id\")
*     .setType({id: \"my_id\", type: \"my_type\"})
* console.log(vc.getType())
* ```
* The code above will print in console the following:
* ```json
* {
*     id: \"my_id\",
*     type: \"my_type\
* }
* ```
* @returns {any} 
*/
  getType(): any;
/**
* # Get Id
* ```javascript
* import { VCredential } from \'caelum_vcdm\';
* let vc = new VCredential(\"my_id\");
* console.log(vc.getId())
* ```
* The code above will print in console the following:
* ```json
* id: \"my_id\
* ```
* @returns {any} 
*/
  getId(): any;
/**
* # Get Issuer
* ```javascript
* import { VCredential } from \'caelum_vcdm\';
* let vc = new VCredential(\"my_id\")
*     .setIssuanceDate(\"2018-06-17T10:03:48Z\"})
* console.log(vc.getIssuanceDate())
* ```
* The code above will print in console the following:
* ```json
* \"2018-06-17T10:03:48Z\
* ```
* @returns {any} 
*/
  getIssuer(): any;
/**
* # Get Claims
* ```javascript
* import { VCredential } from \'caelum_vcdm\';
* let vc = new VCredential(\"my_id\")
*     .setIssuanceDate(\"2018-06-17T10:03:48Z\"})
* console.log(vc.getIssuanceDate())
* ```
* The code above will print in console the following:
* ```json
* \"2018-06-17T10:03:48Z\
* ```
* @returns {any} 
*/
  getClaims(): any;
/**
* # Get Proof
* ```javascript
* import { VCredential } from \'caelum_vcdm\';
* let vc = new VCredential(\"my_id\")
*     .setProof({
*        \"type\": \"RsaSignature2018\",
*        \"created\": \"2018-06-17T10:03:48Z\",
*        \"verificationMethod\": \"did:example:ebfeb1276e12ec21f712ebc6f1c#k1\",
*        \"signatureValue\": \"pY9...Cky6Ed = \
*     })
* console.log(vc.getProof())
* ```
* The code above will print in console the following:
* ```json
* [{
*     \"type\": \"RsaSignature2018\",
*     \"created\": \"2018-06-17T10:03:48Z\",
*     \"verificationMethod\": \"did:example:ebfeb1276e12ec21f712ebc6f1c#k1\",
*     \"signatureValue\": \"pY9...Cky6Ed = \
* }]
* ```
* @returns {any} 
*/
  getProof(): any;
/**
* # Get NonRevocationProof
* ```javascript
* import { VCredential } from \'caelum_vcdm\';
* ```json
* [{
*     \"type\": \"RsaSignature2018\",
*     \"created\": \"2018-06-17T10:03:48Z\",
*     \"verificationMethod\": \"did:example:ebfeb1276e12ec21f712ebc6f1c#k1\",
*     \"signatureValue\": \"pY9...Cky6Ed = \
* }]
* ```
* @returns {any} 
*/
  getNonRevocationProof(): any;
/**
* # Jsonify Verifiable Credential
* `toJSON` method will output the current `VCredential` as a `JsValue` (json object).
* @returns {any} 
*/
  toJSON(): any;
}
/**
*
* # WebAssembly gateway for ZenroomProofs
*/
export class VNonRevocationProof {
  free(): void;
/**
* @returns {VNonRevocationProof} 
*/
  constructor();
/**
* @param {any} json 
* @returns {VNonRevocationProof} 
*/
  static fromJSON(json: any): VNonRevocationProof;
/**
* @returns {any} 
*/
  toJSON(): any;
}
/**
*
* # Webassembly gateway for Verifiable Presentation
*/
export class VPresentation {
  free(): void;
/**
* # Web assembly Constructor
* `VPresentation`\'s constructor will create an empty `VPresentation` object. An empty object
* is not a valid `VPresentation`.
* ```javascript
* import { VPresentation } from \'caelum_vcdm\';
* let vp = new VPresentation(\"my_id\");
* ```
* @param {string} id 
* @returns {VPresentation} 
*/
  constructor(id: string);
/**
* # WebAssembly Constructor `formJSON`
* `VPresentation`\'s constructor will create a `VPresentation` object from input. Input must
* be a JSON object with all properties defined. If no value is wanted for certain property,
* must be empty. This is also true for sub properties (properties of properties).
* ```javascript
* import { VPresentation } from \'caelum_vcdm\';
* let vp_json = {
*    \"@context\": [
*        \"https://www.w3.org/2018/credentials/v1\",
*        \"https://www.w3.org/2018/credentials/examples/v1\
*    ],
*    \"id\": \"did:example:ebfeb1276e12ec21f712ebc6f1c\",
*    \"type\": [
*        \"VerifiableCredential\",
*        \"PersonalInformation\
*    ],
*    \"verifiableCredential\": [
*        {
*            \"@context\": [
*                \"https://www.w3.org/2018/credentials/v1\",
*                \"https://www.w3.org/2018/credentials/examples/v1\
*            ],
*            \"id\": \"http://example.com/credentials/4643\",
*            \"type\": [
*                \"VerifiableCredential\",
*                \"PersonalInformation\
*            ],
*            \"issuer\": \"did:example:ebfeb1276e12ec21f712ebc6f1c\",
*            \"issuanceDate\": \"2010-01-01T19:73:24Z\",
*            \"credentialStatus\": {
*                \"id\": \"StatusID\",
*                \"type\": \"available\
*            },
*            \"credentialSubject\": [{
*                \"type\": \"did:example:abfab3f512ebc6c1c22de17ec77\",
*                \"name\": \"Mr John Doe\",
*                \"mnumber\": \"77373737373A\",
*                \"address\": \"10 Some Street, Anytown, ThisLocal, Country X\",
*                \"birthDate\": \"1982-02-02-00T00:00Z\
*            }],
*            \"proof\": {
*                \"type\": \"RsaSignature2018\",
*                \"created\": \"2018-06-17T10:03:48Z\",
*                \"verificationMethod\": \"did:example:ebfeb1276e12ec21f712ebc6f1c#k1\",
*                \"signatureValue\": \"pY9...Cky6Ed = \
*            }
*        }
*    ],
*    \"proof\": {
*        \"type\": \"RsaSignature2018\",
*        \"created\": \"2018-06-17T10:03:48Z\",
*        \"verificationMethod\": \"did:example:ebfeb1276e12ec21f712ebc6f1c#k1\",
*        \"signatureValue\": \"pY9...Cky6Ed = \
*    }
* };
* let ver_pres = VPresentation.fromJSON(vp_json);
* ```
* @param {any} json 
* @returns {VPresentation} 
*/
  static fromJSON(json: any): VPresentation;
/**
* # Set context
* By default `context` will be set to
* ```json
* {
*     context: [
*         \"https://www.w3.org/2018/credentials/v1\",
*         \"https://www.w3.org/2018/credentials/examples/v1\
*     ]
* }
* ```
* To overwrite default `context`s use method `setContext()` can be used.
* ```javascript
* import { VPresentation } from \'caelum_vcdm\';
* let vp = new VPresentation(\"my_id\")
*     .setContext(\"new_context\");
* ```
* @param {string} c 
* @returns {VPresentation} 
*/
  setContext(c: string): VPresentation;
/**
* # Set Id
* By default `id` will be set by the constructor.
* To overwrite default `id` use method `setId()` can be used.
* ```javascript
* import { VPresentation } from \'caelum_vcdm\';
* let vp = new VPresentation(\"my_id\")
*     .setId(\"my_id\");
* ```
* @param {string} id 
* @returns {VPresentation} 
*/
  setId(id: string): VPresentation;
/**
* # Set Type
* By default `type` will be set to
* ```json
* {
*     type: [
*         \"VerifiableCredential\",
*         \"PersonalInformation\
*     ]
* }
* ```
* To overwrite default `type`s use method `setType()` can be used.
* ```javascript
* import { VPresentation } from \'caelum_vcdm\';
* let vp = new VPresentation(\"my_id\")
*     .setType(\"new_type\");
* ```
* @param {string} t 
* @returns {VPresentation} 
*/
  setType(t: string): VPresentation;
/**
* # Set Verifiable Credential
* By default `verifiableCredential` will be an empty vector `Vec::new()` representing
* `Vec<VerifiableCredential>. A array of json objects representing a `VCredential`.
* ```javascript
* import { VPresentation } from \'caelum_vcdm\';
* let vp = new VPresentation(\"my_id\")
*     // Variable `vc` represents a `VCredential` object
*     .addVerifiableCredential(vp.toJSON())
* ```
* Equivalent to
* ```javascript
* import { VPresentation } from \'caelum_vcdm\';
* let vp = new VPresentation(\"my_id\")
*     .addVerifiableCredential({
*         \"@context\": [
*             \"https://www.w3.org/2018/credentials/v1\",
*             \"https://www.w3.org/2018/credentials/examples/v1\
*         ],
*         \"id\": \"http://example.com/credentials/4643\",
*         \"type\": [
*             \"VerifiableCredential\",
*             \"PersonalInformation\
*         ],
*         \"issuer\": \"did:example:ebfeb1276e12ec21f712ebc6f1c\",
*         \"issuanceDate\": \"2010-01-01T19:73:24Z\",
*         \"credentialStatus\": {
*             \"id\": \"StatusID\",
*             \"type\": \"available\
*         },
*         \"credentialSubject\": [{
*             \"type\": \"did:example:abfab3f512ebc6c1c22de17ec77\",
*             \"name\": \"Mr John Doe\",
*             \"mnumber\": \"77373737373A\",
*             \"address\": \"10 Some Street, Anytown, ThisLocal, Country X\",
*             \"birthDate\": \"1982-02-02-00T00:00Z\
*         }],
*         \"proof\": {
*             \"type\": \"RsaSignature2018\",
*             \"created\": \"2018-06-17T10:03:48Z\",
*             \"verificationMethod\": \"did:example:ebfeb1276e12ec21f712ebc6f1c#k1\",
*             \"signatureValue\": \"pY9...Cky6Ed = \
*         }
*     });
* ```
* @param {any} json 
* @returns {VPresentation} 
*/
  setVerifiableCredential(json: any): VPresentation;
/**
* # Set Proof
* /// By default `proof` will be set to empty.
* To overwrite default `proof` use method `setProof()` can be used.
* ```javascript
* import { VPresentation } from \'caelum_vcdm\';
* let vp = new VPresentation(\"my_id\")
*     .setProof({
*         \"type\": \"RsaSignature2018\",
*         \"created\": \"2018-06-17T10:03:48Z\",
*         \"verificationMethod\": \"did:example:ebfeb1276e12ec21f712ebc6f1c#k1\",
*         \"signatureValue\": \"pY9...Cky6Ed = \
*     });
* ```
* @param {any} p 
* @returns {VPresentation} 
*/
  setProof(p: any): VPresentation;
/**
* # Add context
* By default `context` will be set to
* ```json
* {
*     context: [
*         \"https://www.w3.org/2018/credentials/v1\",
*         \"https://www.w3.org/2018/credentials/examples/v1\
*     ]
* }
* ```
* To **add** to the default `context` array use method `addContext()` can be used.
* ```javascript
* import { VPresentation } from \'caelum_vcdm\';
* let vp = new VPresentation(\"my_id\")
*     .addContext(\"another_context\");
* ```
* @param {string} c 
* @returns {VPresentation} 
*/
  addContext(c: string): VPresentation;
/**
* # Add Type
* By default `type` will be set to:
* ```json
* {
*     type: [
*         \"VerifiableCredential\",
*         \"PersonalInformation\
*     ]
* }
* ```
* Once a property `type` is set, one may want to add to the array. To do so use method
* `addType()`.
* ```javascript
* import { VPresentation } from \'caelum_vcdm\';
* let vp = new VPresentation(\"my_id\")
*     .addType(\"new_type\");
* ```
* @param {string} t 
* @returns {VPresentation} 
*/
  addType(t: string): VPresentation;
/**
* # Add Verifiable Credential
* By default `verifiableCredential` create an empty vector. In order to add/push more
* `verifiableCredential`s to the array use method `addVerifiableCredential()`.
* ```javascript
* import { VPresentation } from \'caelum_vcdm\';
* let vp = new VPresentation(\"my_id\")
*     .addVerifiableCredential({
*         \"@context\": [
*             \"https://www.w3.org/2018/credentials/v1\",
*             \"https://www.w3.org/2018/credentials/examples/v1\
*         ],
*         \"id\": \"http://example.com/credentials/4643\",
*         \"type\": [
*             \"VerifiableCredential\",
*             \"PersonalInformation\
*         ],
*         \"issuer\": \"did:example:ebfeb1276e12ec21f712ebc6f1c\",
*         \"issuanceDate\": \"2010-01-01T19:73:24Z\",
*         \"credentialStatus\": {
*             \"id\": \"StatusID\",
*             \"type\": \"available\
*         },
*         \"credentialSubject\": [{
*             \"type\": \"did:example:abfab3f512ebc6c1c22de17ec77\",
*             \"name\": \"Mr John Doe\",
*             \"mnumber\": \"77373737373A\",
*             \"address\": \"10 Some Street, Anytown, ThisLocal, Country X\",
*             \"birthDate\": \"1982-02-02-00T00:00Z\
*         }],
*         \"proof\": {
*             \"type\": \"RsaSignature2018\",
*             \"created\": \"2018-06-17T10:03:48Z\",
*             \"verificationMethod\": \"did:example:ebfeb1276e12ec21f712ebc6f1c#k1\",
*             \"signatureValue\": \"pY9...Cky6Ed = \
*         }
*     });
* ```
* @param {any} vc 
* @returns {VPresentation} 
*/
  addVerifiableCredential(vc: any): VPresentation;
/**
* # Get Context
* ```javascript
* import { VPresentation } from \'caelum_vcdm\';
* let vp = new VPresentation(\"my_id\")
* console.log(vp.getContext())
* ```
* The code above will print in console the following:
* ```json
* {
*     context: [
*         \"https://www.w3.org/2018/credentials/v1\",
*         \"https://www.w3.org/2018/credentials/examples/v1\
*     ]
* }
* ```
* @returns {any} 
*/
  getContext(): any;
/**
* # Get Verifiable Credential
* ```javascript
* import { VPresentation } from \'caelum_vcdm\';
* let vp = new VPresentation(\"my_id\")
*     addVerifiableCredential(serde_json::from_str(r#\"{
*         \"@context\": [
*             \"https://www.w3.org/2018/credentials/v1\",
*             \"https://www.w3.org/2018/credentials/examples/v1\
*         ],
*         \"id\": \"http://example.com/credentials/4643\",
*         \"type\": [
*             \"VerifiableCredential\",
*             \"PersonalInformation\
*         ],
*         \"issuer\": \"did:example:ebfeb1276e12ec21f712ebc6f1c\",
*         \"issuanceDate\": \"2010-01-01T19:73:24Z\",
*         \"credentialStatus\": {
*             \"id\": \"StatusID\",
*             \"type\": \"available\
*         },
*         \"credentialSubject\": [{
*             \"type\": \"did:example:abfab3f512ebc6c1c22de17ec77\",
*             \"name\": \"Mr John Doe\",
*             \"mnumber\": \"77373737373A\",
*             \"address\": \"10 Some Street, Anytown, ThisLocal, Country X\",
*             \"birthDate\": \"1982-02-02-00T00:00Z\
*         }],
*         \"proof\": [{
*             \"type\": \"RsaSignature2018\",
*             \"created\": \"2018-06-17T10:03:48Z\",
*             \"verificationMethod\": \"did:example:ebfeb1276e12ec21f712ebc6f1c#k1\",
*             \"signatureValue\": \"pY9...Cky6Ed = \
*         }]
*     }\"#).unwrap());
* console.log(vp.getVerifialbleCredential());
* ```
* The code above will print in console the following:
* ```json
* {
*         \"@context\": [
*             \"https://www.w3.org/2018/credentials/v1\",
*             \"https://www.w3.org/2018/credentials/examples/v1\
*         ],
*         \"id\": \"http://example.com/credentials/4643\",
*         \"type\": [
*             \"VerifiableCredential\",
*             \"PersonalInformation\
*         ],
*         \"issuer\": \"did:example:ebfeb1276e12ec21f712ebc6f1c\",
*         \"issuanceDate\": \"2010-01-01T19:73:24Z\",
*         \"credentialStatus\": {
*             \"id\": \"StatusID\",
*             \"type\": \"available\
*         },
*         \"credentialSubject\": [{
*             \"type\": \"did:example:abfab3f512ebc6c1c22de17ec77\",
*             \"name\": \"Mr John Doe\",
*             \"mnumber\": \"77373737373A\",
*             \"address\": \"10 Some Street, Anytown, ThisLocal, Country X\",
*             \"birthDate\": \"1982-02-02-00T00:00Z\
*         }],
*         \"proof\": {
*             \"type\": \"RsaSignature2018\",
*             \"created\": \"2018-06-17T10:03:48Z\",
*             \"verificationMethod\": \"did:example:ebfeb1276e12ec21f712ebc6f1c#k1\",
*             \"signatureValue\": \"pY9...Cky6Ed = \
*         }
*     }
* }
* ```
* @returns {any} 
*/
  getVerifialbleCredential(): any;
/**
* # Get Type
* ```javascript
* import { VPresentation } from \'caelum_vcdm\';
* let vp = new VPresentation(\"my_id\")
*     .setType(\"my_type\")
*     .addType(\"my_type2\")
* console.log(vp.getType())
* ```
* The code above will print in console the following:
* ```json
* [\"my_type\", \"my_type2\"]
* ```
* @returns {any} 
*/
  getType(): any;
/**
* # Get Id
* ```javascript
* import { VPresentation } from \'caelum_vcdm\';
* let vp = new VPresentation(\"my_id\");
* console.log(vp.getId())
* ```
* The code above will print in console the following:
* ```json
* id: \"my_id\
* ```
* @returns {any} 
*/
  getId(): any;
/**
* # Get Proof
* ```javascript
* import { VPresentation } from \'caelum_vcdm\';
* let vp = new VPresentation(\"my_id\")
*     .setProof({
*        \"type\": \"RsaSignature2018\",
*        \"created\": \"2018-06-17T10:03:48Z\",
*        \"verificationMethod\": \"did:example:ebfeb1276e12ec21f712ebc6f1c#k1\",
*        \"signatureValue\": \"pY9...Cky6Ed = \
*     })
* console.log(vp.getProof())
* ```
* The code above will print in console the following:
* ```json
* {
*     \"type\": \"RsaSignature2018\",
*     \"created\": \"2018-06-17T10:03:48Z\",
*     \"verificationMethod\": \"did:example:ebfeb1276e12ec21f712ebc6f1c#k1\",
*     \"signatureValue\": \"pY9...Cky6Ed = \
* }
* ```
* @returns {any} 
*/
  getProof(): any;
/**
* # Jsonify Verifiable Presentation
* `toJSON` method will output the current `VPresentation` as a `JsValue` (json object).
* @returns {any} 
*/
  toJSON(): any;
}
/**
*
* # WebAssembly gateway for Proofs
*/
export class VProof {
  free(): void;
/**
* @returns {VProof} 
*/
  constructor();
/**
* @param {any} json 
* @returns {VProof} 
*/
  static fromJSON(json: any): VProof;
/**
* @param {any} json_claim 
* @param {Uint8Array} keys 
* @returns {VProof} 
*/
  static fromClaimAndKeys(json_claim: any, keys: Uint8Array): VProof;
/**
* @returns {any} 
*/
  toJSON(): any;
}
