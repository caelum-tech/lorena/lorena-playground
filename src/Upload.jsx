import React from 'react'
import nextId from 'react-id-generator'
import './Upload.css'

/**
 * Select image file for upload, display in img, and propagate to parent
 */
class Upload extends React.Component {
  htmlId = nextId()

  constructor(props){
    super(props)
    this.state = {
      file: null
    }
    this.handleChange = this.handleChange.bind(this)
  }

  /**
   * Pull the data from the file control for assignment to parent
   * @param {*} file
   */
  getFileData(file) {
    return new Promise(function(resolve, reject) {
        var reader = new FileReader()
        reader.onload = function() {
          resolve(reader.result)
        }
        reader.onerror = reject
        reader.readAsDataURL(file)
    })
  }

  /**
   * Upload the local state and parent property when a new file is loaded into the file input
   * @param {*} event
   */
  handleChange(event) {
    // when the dialog is cancelled, this gets called with zero files
    if (event.target.files.length > 0) {
      const file = event.target.files[0]
      // update the local property, which is displayed
      this.setState({ file: URL.createObjectURL(file) })
      // update the parent property
      this.getFileData(file).then((data) => {
        this.props.update(data)
      })
    }
  }

  /**
   * Render:
   *   a nearly invisible input for the file,
   *   a clickable, tab-stop label to select that file, and
   *   an image which displays the file selected
   */
  render() {
    return (
      <div>
        <input type="file" name={this.props.name} id={this.htmlId} className="inputfile" onChange={this.handleChange}/>
        <label htmlFor={this.htmlId}>{this.props.label}</label>
        <img width="100%" alt="" src={this.state.file}/>
      </div>
    )
  }
}

export default Upload