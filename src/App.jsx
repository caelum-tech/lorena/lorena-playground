/* eslint-disable jsx-a11y/anchor-is-valid */
import keyring from '@polkadot/ui-keyring'
import { ApiPromise, WsProvider } from '@polkadot/api'
import { Message, Step, Select, Button, Header, Form, Input, Card, Container, Dimmer, Loader, Grid, Image, Menu, Confirm } from 'semantic-ui-react'
import DeveloperConsole from './DeveloperConsole'
import NodeInfo from './NodeInfo'
import 'semantic-ui-css/semantic.min.css'
import React, { useState, useEffect, createRef } from 'react'
import QrReader from 'react-qr-reader'
import Zen from '@caelum-tech/zenroom-lib'
import { u8aToU8a } from '@polkadot/util'
import * as localForage from 'localforage'
import ReactJson from 'react-json-view'
import Upload from './Upload'
const MatrixClient = require('@caelum-tech/lorena-matrix-client')
const vcr = require('@caelum-tech/lorena-vcr')
const { mnemonicGenerate, mnemonicToSeed, naclKeypairFromSeed } = require('@polkadot/util-crypto')
const { encodeAddress } = require('@polkadot/keyring')
const CryptoJS = require('crypto-js')

const matrixLobby = process.env.REACT_APP_MATRIX_LORENA_LOBBY
const matrixServerUrl = process.env.REACT_APP_MATRIX_SERVER_URL

export default function App() {
  const WS_PROVIDER = process.env.REACT_APP_SUBSTRATE_WS
  const [api, setApi] = useState()
  const [apiReady, setApiReady] = useState()
  const [id_credentials, setCredentials] = useState(null)
  const [credentialEmail, setCredentialEmail] = useState('')
  const [credentialFirstName, setCredentialFirstName] = useState('')
  const [credentialLastName, setCredentialLastName] = useState('')
  const [credentialPostalCode, setCredentialPostalCode] = useState('')
  const [id_name, setName] = useState('')
  const [id_pass, setPass] = useState('')
  const [id_rep_pass, setRepPass] = useState('')
  const [idBack, setIdBack] = useState('')
  const [idFront, setIdFront] = useState('')
  const [user, setUser] = useState(null)
  const [addUser, setAddUser] = useState(false)
  const [loading /*, setLoading */] = useState(false)
  const [error, setError] = useState(0)
  const [step, setStep] = useState(0)
  const [storageCredentialsLoaded, setStorageCredentialsLoaded] = useState(false)
  const [showQR, setShowQR] = useState(false)
  const [localforage /*, setlocalforage */] = useState(localForage.createInstance({}))
  const [selectedCredentials, setSelectedCredentials] = useState([])
  const [showSelectedCredentials, setShowSelectedCredentials] = useState(false)
  const [qrData, setQrData] = useState(null)

  const options = [
    { text: 'Polkadot', value: 'polkadot' },
    { text: 'uPort (Ethereum)', value: 'uport' },
    { text: 'Evernym (Indy)', value: 'evernym' },
    { text: 'Cosmos', value: 'cosmos' },
    { text: 'RIF (Bitcoin)', value: 'rif' },
  ]

  useEffect( () => {

    const provider = new WsProvider(WS_PROVIDER)
    const TYPES = {}

    // Load User from `localforage`
    const loadUser = () => {
      localforage.getItem('user')
        .then( function (user) {
          setUser(JSON.parse(user))
          return JSON.parse(user)
        }).catch( function (err) {
        console.error(err, ': User not found')
      })
    }

    // 1. Load all accounts into Keyring
    keyring.loadAll({
      isDevelopment: true
    })

    // 2. Load Current User. If any
    loadUser()

    // 3. Load Substrate API
    ApiPromise.create({ provider, types: TYPES})
      .then(api => {
        setApi(api)
        api.isReady.then(() => {
          setApiReady(true)

          // Filter some event from feed
          api.query.system.events(events => {
            events.forEach(record => {
              const { event } = record
              const types = event.typeDef

              let eventName = `${event.section}:${event.method}`
              let params = event.data.map((data, index) => { return `${types[index].type}: ${data.toString()}`})
              // A new DID is created for user's address
              if (eventName === 'radices:DidCreated') {
                localforage.getItem('user')
                  .then(function (user) {
                    user = JSON.parse(user)
                    user.did = params[1]
                    setUser(user)
                    localforage.setItem('user', JSON.stringify(user))
                      .then(function () {
                        console.log('User added to local storage')
                      })
                      .catch(function (err) {
                        console.error(err)
                      })
                    setAddUser(false)
                    setStep(2)
                  })
              }
            })
          })
        })
      })
      .catch(e => console.error(e))
  }, [WS_PROVIDER, localforage])

  // Initial loader function.
  const loader = function(text) {
    return (
      <Dimmer active>
        <Loader size="small">{text}</Loader>
      </Dimmer>
    )
  }

  if (!apiReady) {
    return loader('Connecting to the blockchain')
  }

  const contextRef = createRef()

  // Show DID Info
  const setBlockchain = (event) => {
    if (event === 'polkadot') {
      setStep(1)
    }
    else {
      setError(1)
    }
  }

  // Show Lorena Info
  const radices_info = () => {
    let content = ''
    if (error) {
      content = (
        <Message color='yellow'>
          <b>Not available yet</b><br/>
          We are working to add more Identities to Lorena
        </Message>
      )
    }
    return(
      <div>
        <Card fluid>
          <Header as='h2' icon textAlign='center' style={{ marginTop:'20px', marginBottom: '15px' }}>
            <Image centered size='large' src='/containers.png'/>
            <Header.Content>Lorena Hub</Header.Content>
            <Header.Subheader>
              Interoperable open-source system to connect identities through different blockchains
            </Header.Subheader>
          </Header>
          <Card.Content>
            <Container>
              <Header as='h3'>Welcome to Lorena</Header>
              <Form>
                <Form.Field inline width={6}>
                  <Form.Field
                    control={Select}
                    label='Select SSI Tech'
                    options={options}
                    placeholder='Blockchain'
                    onChange={(_, _data) => setBlockchain(_data.value)}
                  />
                </Form.Field>
                {content}
              </Form>
            </Container>
          </Card.Content>
          <Card.Content>
            <Card.Description>
              Lorena is part of Lorena Ecosystem, an interoperable framework for self-sovereign identities that can be implemented in any decentralized network. It is ready for use by third parties as middleware.
            </Card.Description>
          </Card.Content>
        </Card>
      </div>
    )
  }
  // Clear Identity in the Playground.
  const clearID = () => {
    localforage.removeItem('user').then(function () {
      setUser(null)
    })
    localforage.removeItem('credentials').then(function () {
      setCredentials(null)
    })
  }

  const createKeyPair = async () => {
    const mnemonic = mnemonicGenerate()
    const seed = mnemonicToSeed(mnemonic)
    const { secretKey, publicKey } = naclKeypairFromSeed(seed)
    return {
      mnemonic,
      seed,
      secretKey,
      publicKey
    }
  }

  function string2u8(str) {
    var result = []
    for (var i = 0; i < str.length; i++) {
      result.push(str.charCodeAt(i).toString(10))
    }
    return result
  }

  // Create and save new DID.
  const saveIdentity = async () => {
    setAddUser(true)
    // Key Pair for Substrate
    const { mnemonics: mnemonicS, seed: seedS, secretKey: secretKeyS, publicKey: publicKeyS } = await createKeyPair()
    // Key Pair for Zenroom
    let z = new Zen()
    const alice = await z.newKeyPair('Alice')
    const { public_key: publicKeyZ, private_key: secretKeyZ } = alice.Alice.keypair
    var publicKeyZu8a = u8aToU8a(new Uint8Array(string2u8(publicKeyZ)))//new Uint8Array(string2u8(publicKeyZ))
    await createDID(publicKeyS, publicKeyZu8a)
    // Encrypt keys with password!
    const encryptedSecretS = CryptoJS.Rabbit.encrypt(secretKeyS.toString(), id_pass)

    const _user = {
      handler: id_name,
      password: id_pass,
      // Substrate identity
      seedS,
      mnemonicS,
      secretKeyS: encryptedSecretS.toString(),
      publicKeyS: new Uint8Array(Object.values(publicKeyS)),
      // Zenroom identity
      alice,
      secretKeyZ,
      publicKeyZ: publicKeyZu8a,
      // Address
      addressS: encodeAddress(publicKeyS),
      addressZ: publicKeyZ,
      did: '',
      timestamp: new Date(Date.now()).toISOString()
    }
    setUser(_user)
    localforage.setItem('user', JSON.stringify(_user))
      .then(function () {
        console.log('User added to local storage')
      })
      .catch(function (err) {
        console.error(err)
      })
  }

  // Call Blockchain and add a new DID.
  const createDID = async (keyS, keyZ) => {
    const accounts = keyring.getPairs()
    let transaction = await api.tx.radices.createDid(keyS, keyZ)
    transaction
      .signAndSend(accounts[0], ({ status }) => {
        if (status.isFinalized) {
          console.log(`Completed at block hash #${status.asFinalized.toString()}`)
        } else {
          console.log(`Current transaction status: ${status.type}`)
        }
      })
      .catch(e => {
        console.log(':( transaction failed')
        console.error('ERROR:', e)
      })
  }

  const onSelectedCredentials = () => {
    const matrixClient = new MatrixClient()
    matrixClient.authenticateAsGuest(matrixServerUrl, matrixLobby, 'authenticate!', qrData, selectedCredentials)
    setShowSelectedCredentials(false)
  }

  const onScanQR = (data) => {
    if (data) {
      try {
        const dataJson = vcr.decodeVCR(data)
        if (id_credentials == null) {
          console.log('No credentials created!')
          return
        }
        let sCred = []
        for (var cred of id_credentials) {
          if ( cred.type.includes(dataJson.type)) sCred.push(cred)
        }
        if (sCred.length === 0){
          console.log('No credential with type ', dataJson.type)
          alert('No credential with type ' + dataJson.type)
          return
        }
        setSelectedCredentials(sCred)
        setShowSelectedCredentials(true)
        setQrData(dataJson)
        setShowQR(false)
      }
      catch (e) {
        console.error(e)
      }
    }
  }

  const createShowCredentials = () => {
    let sCred = []
    for (var cred of selectedCredentials) {
      sCred.push(cred.claims[0].credentialSubject[0].email)
    }
    return sCred.toString()
  }

  const did_info = () => {
    if((id_credentials === null || id_credentials === []) && storageCredentialsLoaded === false) {
      loadCredentials()
      setStorageCredentialsLoaded(true)
    }

    let content = ''
    if (addUser) {
      content = (<Loader active inline='centered' />)
    }
    else if (user !== null && user.handler !== false) {
      let qrCode
      if (showQR) {
        qrCode = <div>
          <QrReader
            delay={800}
            onError={()=> {console.error('ERROR')}}
            onScan={onScanQR}
            style={{ width: '100%' }}
          />
        </div>
      } else {
        qrCode = ''
      }
      content = (
        <Card.Content>
          <Confirm
            content={ createShowCredentials() }
            open={ (showSelectedCredentials) }
            onCancel={() => {setShowSelectedCredentials(false)}}
            onConfirm={onSelectedCredentials}
          />
          {qrCode}
          <div>handler: @{user.handler}</div>
          <div>Address: {user.addressZ}</div>
          <div>{user.did}</div>
          <div><a href="#" onClick={ () => {clearID()}}>Clear Identity</a></div>
          <br/><Button onClick={ () => { showQR ? setShowQR(false) : setShowQR(true) } }>Verify credential</Button>
        </Card.Content>
      )
    }
    else {
      content = (
        <Card.Content>
          <Container>
            <Header as='h3'>First you need to create a new DID in the Blockchain</Header>
            <Form onSubmit={ saveIdentity }>
              <Form.Field inline width={6}>
                <label>User handler</label>
                <Input onChange={(_, data) => {setName(data.value)}} label="@" placeholder="identity" state="id_name" type="text"/>
              </Form.Field>

              <Form.Field inline width={6}>
                <label>User password</label>
                <Input type="password" onChange={(_, data) => {setPass(data.value)}} state="id_pass"/>
              </Form.Field>

              <Form.Field inline width={6}>
                <label>Repeat password</label>
                <Input type="password" onChange={(_, data) => {setRepPass(data.value)}} state="id_rep_pass"/>
              </Form.Field>

              <Form.Button
                type="submit"
                disabled={id_name.length<3 || id_pass.length<3 || id_rep_pass !== id_pass}>
                Create Identity
              </Form.Button>
            </Form>
          </Container>
        </Card.Content>
      )
    }
    return(
      <div>
        <Card fluid>
          <Header as='h2' icon textAlign='center'>
            <Image centered size='large' src='/containers.png'/>
            <Header.Content>Global Unique Identifiers - DIDs</Header.Content>
            <Header.Subheader>
              Manage your account settings and set email preferences
            </Header.Subheader>
          </Header>
          {content}
          <Card.Content>
            <Card.Description>
              <b>Decentralized Identifiers (DIDs)</b> are a new type of identifier for verifiable, decentralized digital identity. These new identifiers are designed to enable the controller of a DID to prove control over it and to be implemented independently of any centralized registry, identity provider, or certificate authority
              <br/><a href="https://w3c-ccg.github.io/did-spec/">W3C: Decentralized Identifiers (DIDs) v1.0</a>
              <br/><Button onClick={() => setStep(2)} >Go to Credentials</Button>
            </Card.Description>
          </Card.Content>
        </Card>
      </div>
    )
  }

  /**
   * Construct a DID from the hash object of 'Hash: 0xDEADBEEF
   *
   * @param {String} did The string containing the hash
   */
  const getDidFromDidHash = (did) => {
    return `did:lor:${did.substring(did.search('0x')+2)}`
  }

  /**
   * Construct the credential subject
   *
   * @param {String} credentialType The type of credential to create
   */
  const createCredentialSubject = (credentialType) => {
    if (credentialType === 'CaelumEmailCredential') {
      return {
        'type': credentialType,
        'email': credentialEmail,
        'first': credentialFirstName,
        'last': credentialLastName
      }
    } else if (credentialType === 'CaelumResidencyCredential') {
      return {
        'type': credentialType,
        'idBack': 'idBack.png', // TODO use makeSingleCredentialFile() and submitCredentialForVerification()
        'idFront': 'idFront.png', // TODO use makeSingleCredentialFile() and submitCredentialForVerification()
        'postalCode': credentialPostalCode
      }
    }
  }

  /**
   * Create credential of the specified type
   *
   * @param {String} credentialType The type of credential to create
   */
  const createCredential = async (credentialType) => {
    let z = new Zen()
    let vcdm
    try {
      vcdm = await import('@caelum-tech/caelum-vcdm')
    } catch(err) {
      console.error(`Unexpected error in loadWasm. [Message: ${err.message}]`)
      return undefined
    }
    const now = new Date(Date.now()).toISOString()
    // Create Credential
    const claimJson = {
      'credentialStatus': {
        'id': 'https://lorena.caelumlabs.com/credentials',
        'type': credentialType,
        'currentStatus': 'pending',
        'statusReason': 'self-issued'
      },
      'type': ['VerifiableCredential', credentialType],
      'credentialSubject': [createCredentialSubject(credentialType)],
      'id': getDidFromDidHash(user.did),
      'issuanceDate': now,
      'issuer': getDidFromDidHash(user.did)
    }
    let claim = vcdm.VClaim.fromJSON(claimJson)
    let signedMsg = await z.signMessage('Alice', { Alice: user.alice.Alice }, JSON.stringify(claim.toJSON()))
    let zproofJson = {
      type: credentialType,
      created: now,
      verificationMethod: user.addressZ,
      signatureValue: user.addressZ,
      zenroom: signedMsg.Alice
    }
    const proof = vcdm.VNonRevocationProof.fromJSON(zproofJson)
    const vc = new vcdm.VCredential(getDidFromDidHash(user.did))
         .setIssuer(getDidFromDidHash(user.did))
         .addClaim(claim)
         .addType(credentialType)
         .addNonRevocationProof(proof)
    return vc.toJSON()
  }

  /**
   * Receive credential through Matrix
   *
   * @param {Object} event Information on the matrix event
   * @param {Object} data Metadata passed along: the MatrixClient
   */
  const receiveCredential = (event, data) => {
    const body = event.content.body
    let credential
    console.log('Matrix EVENT:', event)
    try {
      credential = JSON.parse(body)
    } catch (e) {
        return
    }

    if (credential) {
      // The proof will be included in the credential
      let cred
      if( id_credentials === null || id_credentials === [] ) {
        cred = [credential]
      } else {
        cred = id_credentials
        // TODO: Look for specific credential received (NOT the last one)
        cred[cred.length - 1] = credential
      }

      setCredentials(cred)
      saveCredentials(cred)

      // End the session as we have our credentials and need nothing further
      const matrixClient = data
      matrixClient.leave(event.room_id)
      matrixClient.close()
    } else {
      // Other messages sent by the daemon (email sent, errors, etc)
      console.log(body)
    }
  }

  /**
   * Saves credential to state
   *
   * @param {JSON} credential The credential to save
   */
  const addCredentialToState = (credential) => {
    let credentials = []
    if (id_credentials === null ) credentials = [credential]
    else {
      credentials = id_credentials
      credentials.push(credential)
    }
    setCredentials(credentials)
  }

  /**
   * Resets state for values used in credential form
   *
   * @param {JSON} credentialType The credential to send
   */
  const resetState = (credentialType) => {
    if ('CaelumEmailCredential' === credentialType) {
      setCredentialFirstName('')
      setCredentialLastName('')
      setCredentialEmail('')
    } else if ('CaelumResidencyCredential' === credentialType) {
      setCredentialPostalCode('')
    }
  }

  /**
   * Prepares credential for sending to Matrix
   *
   * @param {JSON} credentialType The credential to send
   * @param {Object} e The form
   */
  const sendCredential = async (credentialType, e) => {
    // Clean form
    e.target.querySelectorAll('input').forEach(function(input){input.value = ''})
    // Send Verifiable Credential to BOT.
    // Credential is in state variable `id_credentials`
    const credential = await createCredential(credentialType)
    // Save credential to STATE
    addCredentialToState(credential)
    resetState(credentialType)

    await sendCredentialToMatrix(credential)
  }

  /**
   * Sends the VC to the verifier bot via Matrix
   *
   * @param {JSON} credential The credential to send
   */
  const sendCredentialToMatrix = async (credential) => {
    const matrixClient = new MatrixClient()

    // Creates guest, joins lobby, sends trigger keyword, waits for invitation
    // to private room, sends credential, specifies callback for results
    await matrixClient.submitCredentialForVerificationAsGuest(
      matrixServerUrl,
      matrixLobby,
      'verifyCredential!',
      credential,
      receiveCredential
    )
  }

  /**
   * Save Credential to local storage
   *
   * @param {JSON} credentials The credentials to save
   */
  const saveCredentials = (credentials) => {
    // Credential is in state variable `id_credentials`
    localforage.setItem('credentials', JSON.stringify(credentials))
      .then(function () {
        console.log('Credential added to local storage')
      })
      .catch(function (err) {
        console.error(err)
      })
  }

  /**
   * Load credentials from local storage
   */
  const loadCredentials = () => {
    localforage.getItem('credentials')
      .then( function (cred) {
        setCredentials(JSON.parse(cred))
      }).catch( function (err) {
        console.error(err, ': User not found')
      })
  }

  const vc_info = () => {
    let content
    if (user === null) {
      content = (
        <Card.Content textAlign="center">
          First you must <a onClick={() => setStep(1)} >Create a DID</a> to associate Credentials to it
        </Card.Content>
      )
    }
    else if (loading) {
      content = ( <Loader active inline='centered' />)
    }
    else {
      content = (
        <Container>
          <Header as='h3'>Add a new Credential about yourself</Header>
          <Card.Group stackable itemsPerRow={2}>
            <Card fluid>
              <Card.Content>
                <Container>
                  <Header as='h3'>Verify your email address</Header>
                  <Form onSubmit={ (e) =>{sendCredential('CaelumEmailCredential', e)} }>
                    <Form.Field>
                      <label>Your email</label>
                      <Input id="credentialEmail" onChange={(_, data) => {setCredentialEmail(data.value)}} placeholder="email" state="credentialEmail" type="text"/>
                    </Form.Field>
                    <Form.Field>
                      <label>First name</label>
                      <Input id="credentialFirstName" onChange={(_, data) => {setCredentialFirstName(data.value)}} placeholder="first name" state="credentialFirstName" type="text"/>
                    </Form.Field>
                    <Form.Field>
                      <label>Last name</label>
                      <Input id="credentialLastName" onChange={(_, data) => {setCredentialLastName(data.value)}} placeholder="last name" state="credentialLastName" type="text"/>
                    </Form.Field>
                    <Form.Button primary type="submit" disabled={credentialEmail.length<3} >
                      Create new VC
                    </Form.Button>
                  </Form>
                </Container>
              </Card.Content>
            </Card>
            <Card fluid>
              <Card.Content>
                <Container>
                  <Header as='h3'>Verify your Residency</Header>
                  <Form onSubmit={ (e) =>{sendCredential('CaelumResidencyCredential', e)} }>
                    <Form.Field>
                      <Upload label='Choose ID front image' name='idFront' update={setIdFront} />
                    </Form.Field>
                    <Form.Field>
                      <Upload label='Choose ID back image' name='idBack' update={setIdBack} />
                    </Form.Field>
                    <Form.Field>
                      <label>Postal Code</label>
                      <Input id="credentialPostalCode" onChange={(_, data) => {setCredentialPostalCode(data.value)}} placeholder="postal code" state="credentialPostalCode" type="text"/>
                    </Form.Field>
                    <Form.Button primary type="submit" disabled={!residencyReady()} >
                      Create new VC
                    </Form.Button>
                  </Form>
                </Container>
              </Card.Content>
            </Card>
          </Card.Group>
        </Container>
      )
    }
    let rendered_credentials
    if(id_credentials === null || id_credentials.length === 0){
      rendered_credentials = <span>No existing user's verified credentials</span>
    }else{
      rendered_credentials = id_credentials.map(function(cred, i) {
        return <span key={i}>
          <div id={'credential' + i} state={'id_credentials'}>
            <ReactJson src={cred} />
          </div>
          </span>
      })
    }

    return (
      <div >
        <Card fluid>
          <Card.Content>
            <Header as='h2' icon textAlign='center' style={{ marginTop:'35px' }}>
              <Image centered size='large' src='/seeds.png'/>
              <Header.Content>Verifiable Credentials - DIDs</Header.Content>
              <Header.Subheader>
                Manage your online identity from a single identifier
              </Header.Subheader>
            </Header>
          </Card.Content>
          <Card.Content>
            <Card.Description>
              <b>Credentials</b> are a part of our daily lives; driver's licenses are used to assert that we are capable of operating a motor vehicle, university degrees can be used to assert our level of education, and government-issued passports enable us to travel between countries. This specification provides a mechanism to express these sorts of credentials on the Web in a way that is cryptographically secure, privacy respecting, and machine-verifiable.
              <br/><a href="https://www.w3.org/TR/vc-data-model/">W3C: Verifiable Credentials (VC) v1.0</a>
            </Card.Description>
          </Card.Content>
          {content}
          <Card.Content >
            <Card.Description >
              <h3>User Credentials</h3>
              <Card.Content>
          </Card.Content>
              <br/>
              {rendered_credentials}
            </Card.Description>
          </Card.Content>
        </Card>
      </div>
    )
  }

  const residencyReady = () => {
    return ( credentialPostalCode.length >= 5 && idFront.length > 0 && idBack.length > 0)
  }

  const show_info = () => {
    switch (step) {
      default:
      case 0: return radices_info()
      case 1: return did_info()
      case 2: return vc_info()
    }
  }

  return (
    <div ref={contextRef}>
      <Container>

        {/********** MENU TOP **********/}
        <Menu fixed='top' inverted>
          <Container>
            <Menu.Item as='a' onClick={() => setStep(0)} header>
              <Image size='mini' src='/logo.png' style={{ marginRight: '1.5em' }} />
              Radices Playground
            </Menu.Item>
          </Container>
        </Menu>

        {/********** MAIN CONTAINER - 2 rows**********/}
        <Container style={{ marginTop: '7em' }}>
          <Grid columns={2} stackable>
            <Grid.Column width={4}>
              <Step.Group vertical>
                <Step active={(step===0)} onClick={() => setStep(0)} >
                  < Image size='mini' src='/containers.png' style={{marginRight:'10px'}}/>
                  <Step.Content>
                    <Step.Title>Lorena</Step.Title>
                    <Step.Description>Choose Blockchain</Step.Description>
                  </Step.Content>
                </Step>

                <Step active={(step===1)} onClick={() => setStep(1)} >
                  < Image size='mini' src='/containers.png' style={{marginRight:'10px'}}/>
                  <Step.Content>
                    <Step.Title>DIDs</Step.Title>
                    <Step.Description>Global Identifier</Step.Description>
                  </Step.Content>
                </Step>

                <Step active={(step===2)} onClick={() => setStep(2)} >
                  <Image size='mini' src='/seeds.png' style={{marginRight:'10px'}} />

                  <Step.Content>
                    <Step.Title>Credentials</Step.Title>
                    <Step.Description>Name, Email</Step.Description>
                  </Step.Content>
                </Step>
              </Step.Group>
              <Card.Group>
                <NodeInfo api={api} />
              </Card.Group>
            </Grid.Column>
            <Grid.Column width={12}>

              {/********** DIDS **********/}
              {show_info()}
            </Grid.Column>
          </Grid>
        </Container>
        <DeveloperConsole api={api} />
      </Container>
    </div>
  )
}
