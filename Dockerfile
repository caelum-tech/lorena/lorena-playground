# specify the node base image with your desired version node:<version>
# https://nodejs.org/es/about/releases/
#FROM node:12.4-alpine
FROM node:10-jessie
LABEL maintainer="Albert Valverde <albert@caelumlabs.com>"
LABEL description="DIDs Playground Docker Image"
# replace this with your application's default port
EXPOSE 3000

# Installing tools
RUN apt-get update
RUN apt-get install -y git bash openssh-client curl grep python vim nmon net-tools
RUN apt-get install -y build-essential lsb-release curl  apt-transport-https

# Copying App repository
COPY . /home/node/app
WORKDIR /home/node/app

RUN yarn install

RUN ./node_modules/@caelum-tech/zenroom-lib/bin/zenroom_modules.sh

ENTRYPOINT [ "yarn", "start" ]
